<?xml version="1.0" encoding="utf-8"?>
<layout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools">

    <data>

        <variable
            name="eventModel"
            type="com.livefitter.theinsiders.model.EventModel" />

        <import type="android.view.View" />

        <import type="java.lang.String" />

    </data>

    <android.support.design.widget.CoordinatorLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:background="@drawable/bg_insiders"
        android:fitsSystemWindows="true"
        tools:context=".activity.EventDetailActivity">

        <android.support.design.widget.AppBarLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@color/transparent"
            android:elevation="0dp"
            android:theme="@style/AppTheme.AppBarOverlay"
            app:elevation="0dp">

            <android.support.v7.widget.Toolbar
                android:id="@+id/toolbar"
                android:layout_width="match_parent"
                android:layout_height="?attr/actionBarSize"
                android:background="@color/transparent"
                android:elevation="0dp"
                app:layout_scrollFlags="scroll|enterAlways"
                app:popupTheme="@style/AppTheme.PopupOverlay"
                app:title="@{eventModel.title}"
                app:titleTextColor="@color/text_gold" />

        </android.support.design.widget.AppBarLayout>

        <android.support.v4.widget.NestedScrollView
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            app:layout_behavior="@string/appbar_scrolling_view_behavior">

            <FrameLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content">

                <FrameLayout
                    android:layout_width="match_parent"
                    android:layout_height="@dimen/hero_image_height">

                    <ImageView
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:adjustViewBounds="true"
                        android:scaleType="centerCrop"
                        app:imageSrc="@{eventModel.viewEventImageUrl != null &amp;&amp; !eventModel.viewEventImageUrl.isEmpty() ? eventModel.viewEventImageUrl : eventModel.eventImageUrl}"
                        tools:src="@drawable/_wood" />

                    <View
                        android:layout_width="match_parent"
                        android:layout_height="match_parent"
                        android:background="@drawable/bg_gradient_black_btt" />

                </FrameLayout>

                <android.support.v7.widget.CardView
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginEnd="@dimen/activity_horizontal_margin"
                    android:layout_marginStart="@dimen/activity_horizontal_margin"
                    android:layout_marginTop="@dimen/hero_image_height"
                    app:cardBackgroundColor="@color/white"
                    app:cardCornerRadius="@dimen/cardview_corner_radius"
                    app:cardElevation="@dimen/cardview_default_elevation">

                    <android.support.constraint.ConstraintLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:paddingBottom="24dp"
                        android:paddingTop="24dp">

                        <android.support.constraint.Guideline
                            android:id="@+id/gl_vertical"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:orientation="vertical"
                            app:layout_constraintGuide_percent="0.23" />

                        <!-- Start of Date code block -->

                        <ImageView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_gravity="top"
                            android:scaleType="fitCenter"
                            android:src="@drawable/ic_calendar"
                            app:layout_constraintEnd_toStartOf="@+id/bullet_date"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toTopOf="parent" />

                        <View
                            android:id="@+id/bullet_date"
                            android:layout_width="32dp"
                            android:layout_height="0dp"
                            android:layout_gravity="top"
                            android:background="@drawable/event_detail_bulletpoint_new"
                            app:layout_constraintBottom_toBottomOf="@+id/tv_date"
                            app:layout_constraintEnd_toEndOf="@+id/gl_vertical"
                            app:layout_constraintStart_toStartOf="@+id/gl_vertical"
                            app:layout_constraintTop_toTopOf="parent" />

                        <TextView
                            android:id="@+id/tv_date"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_marginStart="8dp"
                            android:gravity="top|start"
                            android:minHeight="@dimen/event_detail_content_min_height"
                            android:paddingBottom="16dp"
                            android:text="@{eventModel.dateTime}"
                            android:textColor="@color/text_black"
                            app:layout_constraintEnd_toEndOf="parent"
                            app:layout_constraintStart_toEndOf="@+id/bullet_date"
                            app:layout_constraintTop_toTopOf="parent"
                            tools:text="20 January 2017, 8:00 PM" />

                        <!-- End of Date code block -->

                        <!-- Start of Address code block -->

                        <ImageView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_gravity="top"
                            android:layout_weight=".16"
                            android:scaleType="fitCenter"
                            android:src="@drawable/ic_map_marker"
                            app:layout_constraintEnd_toStartOf="@+id/bullet_address"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toTopOf="@+id/tv_address" />

                        <View
                            android:id="@+id/bullet_address"
                            android:layout_width="32dp"
                            android:layout_height="0dp"
                            android:layout_gravity="top"
                            android:background="@drawable/event_detail_bulletpoint_new"
                            app:layout_constraintBottom_toBottomOf="@+id/tv_address"
                            app:layout_constraintEnd_toEndOf="@+id/gl_vertical"
                            app:layout_constraintStart_toStartOf="@+id/gl_vertical"
                            app:layout_constraintTop_toTopOf="@+id/tv_address" />

                        <TextView
                            android:id="@+id/tv_address"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_marginStart="8dp"
                            android:gravity="top|start"
                            android:minHeight="@dimen/event_detail_content_min_height"
                            android:paddingBottom="16dp"
                            android:text="@{eventModel.address}"
                            android:textColor="@color/text_black"
                            app:layout_constraintEnd_toEndOf="parent"
                            app:layout_constraintStart_toEndOf="@+id/bullet_address"
                            app:layout_constraintTop_toBottomOf="@+id/tv_date"
                            tools:text="Building, Street ave., City" />

                        <!-- End of Address code block -->

                        <!-- Start of Description code block -->

                        <ImageView
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_gravity="top"
                            android:scaleType="fitCenter"
                            android:src="@drawable/ic_invite"
                            app:layout_constraintEnd_toStartOf="@+id/bullet_description"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toTopOf="@+id/tv_description" />

                        <View
                            android:id="@+id/bullet_description"
                            android:layout_width="32dp"
                            android:layout_height="0dp"
                            android:layout_gravity="top"
                            android:background="@drawable/event_detail_bulletpoint_end_new"
                            app:layout_constraintBottom_toBottomOf="@+id/tv_description"
                            app:layout_constraintEnd_toEndOf="@+id/gl_vertical"
                            app:layout_constraintStart_toStartOf="@+id/gl_vertical"
                            app:layout_constraintTop_toTopOf="@+id/tv_description" />

                        <TextView
                            android:id="@+id/tv_description"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_marginStart="8dp"
                            android:gravity="top|start"
                            android:minHeight="@dimen/event_detail_content_min_height"
                            android:paddingBottom="16dp"
                            android:text="@{eventModel.eventDescription}"
                            android:textColor="@color/text_black"
                            app:layout_constraintEnd_toEndOf="parent"
                            app:layout_constraintStart_toEndOf="@+id/bullet_description"
                            app:layout_constraintTop_toBottomOf="@+id/tv_address"
                            tools:text="Lorem ipsum dolor lorem ipsum dolor lorem ipsum lorem ipsum dolor lorem ipsum dolor" />

                        <!-- End of Description code block -->

                        <Button
                            android:id="@id/event_detail_express_interest_button"
                            android:layout_width="0dp"
                            android:layout_height="wrap_content"
                            android:layout_marginEnd="@dimen/activity_horizontal_margin"
                            android:layout_marginStart="@dimen/activity_horizontal_margin"
                            android:background="@drawable/selector_button_black_solid"
                            android:text="@{eventModel.hasExpressedInterest? @string/label_remove_interest : @string/label_express_interest}"
                            android:textColor="@color/gold"
                            app:layout_constraintEnd_toEndOf="parent"
                            app:layout_constraintStart_toStartOf="parent"
                            app:layout_constraintTop_toBottomOf="@id/tv_description"
                            app:layout_goneMarginTop="@dimen/activity_vertical_margin"
                            tools:text="@string/label_express_interest" />

                    </android.support.constraint.ConstraintLayout>

                </android.support.v7.widget.CardView>

            </FrameLayout>

        </android.support.v4.widget.NestedScrollView>

        <FrameLayout
            android:id="@id/loading_blocker"
            android:layout_width="match_parent"
            android:layout_height="match_parent"
            android:background="@color/black"
            app:layout_behavior="@string/appbar_scrolling_view_behavior"
            tools:visibility="gone" />

    </android.support.design.widget.CoordinatorLayout>

</layout>