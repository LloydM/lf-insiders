package com.livefitter.theinsiders;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by LloydM on 1/30/17
 * for Livefitter
 */

public abstract class BaseFragment extends Fragment {

    public String getClassTag(){
        return this.getClass().getSimpleName();
    }

    /**
     * Called by the parent activity whenever the back button is pressed.
     * Implementing fragments should handle their own logic here
     * and return true if we are letting the parent activity execute
     * its normal {@link AppCompatActivity#onBackPressed() onBackPressed} behavior
     * @return true if we are letting the parent activity execute its normal onBackPressed behavior
     */
    public abstract boolean onBackPressed();
}
