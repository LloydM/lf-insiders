package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.R;

/**
 * Created by LFT-PC-010 on 7/10/2017.
 */

public class WalkthroughItemFragment extends Fragment {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.item_walkthrough, null);

        ImageView imageView = (ImageView) rootView.findViewById(R.id.img_walkthrough);

        int imageResource = getArguments().getInt(AppConstants.KEY_IMAGE_RESOURCE);

        imageView.setImageResource(imageResource);

        return rootView;
    }
}
