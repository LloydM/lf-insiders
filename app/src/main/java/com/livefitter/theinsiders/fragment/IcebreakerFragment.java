package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.controller.IcebreakerController;
import com.livefitter.theinsiders.databinding.FragmentIcebreakerBinding;


/**
 * Created by LloydM on 3/3/17
 * for Livefitter
 */

public class IcebreakerFragment extends BaseFragment {

    IcebreakerController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mController = new IcebreakerController((BaseActivity) getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentIcebreakerBinding binding = FragmentIcebreakerBinding.inflate(inflater, container, false);

        View rootView = binding.getRoot();
        mController.setBinding(binding);
        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch(id){
            case android.R.id.home:
                getActivity().finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
