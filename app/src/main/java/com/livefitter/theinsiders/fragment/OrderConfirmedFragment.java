package com.livefitter.theinsiders.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.controller.OrderConfirmedController;

/**
 * Created by LFT-PC-010 on 7/18/2017.
 */

public class OrderConfirmedFragment extends BaseFragment {

    private OrderConfirmedController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mController = new OrderConfirmedController((BaseActivity) getActivity(), this);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_order_confirmed, container, false);

        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public boolean onBackPressed() {
        return false;
    }
}
