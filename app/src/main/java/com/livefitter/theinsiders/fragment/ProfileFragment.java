package com.livefitter.theinsiders.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.UpdateProfileActivity;
import com.livefitter.theinsiders.controller.ProfileController;
import com.livefitter.theinsiders.databinding.FragmentProfileBinding;
import com.livefitter.theinsiders.model.UserProfile;


/**
 * Created by LloydM on 3/3/17
 * for Livefitter
 */

public class ProfileFragment extends BaseFragment {

    ProfileController mController;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        setRetainInstance(true);

        mController = new ProfileController((BaseActivity) getActivity(), this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentProfileBinding binding = FragmentProfileBinding.inflate(inflater, container, false);

        View rootView = binding.getRoot();
        mController.setBinding(binding);
        mController.initialize(rootView, savedInstanceState);

        return rootView;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_profile, menu);
        mController.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.action_settings:{
                //start activity for result here
                Intent intent = new Intent(getActivity(), UpdateProfileActivity.class);
                intent.putExtra(AppConstants.KEY_USER_PROFILE, getArguments().getParcelable(AppConstants.KEY_USER_PROFILE));
                getActivity().startActivityForResult(intent, AppConstants.REQUEST_CODE_UPDATE_PROFILE);

                return true;
            }

            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    public void updateProfile(UserProfile newUserProfile){

        Log.i(getClassTag(), "updateProfile: " + newUserProfile.toString());

        //update bundle value
        getArguments().putParcelable(AppConstants.KEY_USER_PROFILE, newUserProfile);
        mController.updateUserValues(newUserProfile);

    }

    @Override
    public void onResume() {
        super.onResume();
        AppConstants.FRAG_CURRENT = AppConstants.FRAG_PROFILE;
    }

    @Override
    public void onStop() {
        super.onStop();
        if(mController != null){
            mController.onStop();
        }
    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
