package com.livefitter.theinsiders.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.theinsiders.adapter.viewholder.PastEventViewHolder;
import com.livefitter.theinsiders.adapter.viewholder.UpcomingEventViewHolder;
import com.livefitter.theinsiders.listener.EventClickListener;
import com.livefitter.theinsiders.model.EventModel;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.GrayscaleTransformation;


/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class PastEventsAdapter extends RecyclerView.Adapter<PastEventViewHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<EventModel> mEventList;
    private EventClickListener mClickListener;

    public PastEventsAdapter(Context mContext, ArrayList<EventModel> mEventList, EventClickListener mClickListener) {
        this.mContext = mContext;
        mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mEventList = mEventList;
        this.mClickListener = mClickListener;
    }

    @Override
    public PastEventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return PastEventViewHolder.create(mInflater, parent);
    }

    @Override
    public void onBindViewHolder(PastEventViewHolder holder, int position) {
        EventModel eventModel = mEventList.get(position);

        if(eventModel != null && mClickListener != null){
            holder.bindTo(eventModel, mClickListener);
        }
    }

    @Override
    public int getItemCount() {
        return mEventList != null? mEventList.size() : 0;
    }
}
