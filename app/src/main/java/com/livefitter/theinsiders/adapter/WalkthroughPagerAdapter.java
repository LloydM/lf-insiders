package com.livefitter.theinsiders.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 7/10/2017.
 */

public class WalkthroughPagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> children = new ArrayList<>();

    public WalkthroughPagerAdapter(FragmentManager fm, ArrayList<Fragment> children) {
        super(fm);

        this.children = children;

    }


    @Override
    public Fragment getItem(int position) {
        return children.get(position);
    }

    @Override
    public int getCount() {
        return children.size();
    }
}
