package com.livefitter.theinsiders.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.databinding.ItemCreditCardBinding;
import com.livefitter.theinsiders.listener.CreditCardClickListener;
import com.livefitter.theinsiders.model.CreditCard;

/**
 * Created by LFT-PC-010 on 7/17/2017.
 */

public class CreditCardListViewHolder extends RecyclerView.ViewHolder {

    private ItemCreditCardBinding mBinding;

    public static CreditCardListViewHolder create(LayoutInflater inflater, ViewGroup parent){

        ItemCreditCardBinding binding = ItemCreditCardBinding.inflate(inflater, parent, false);

        return new CreditCardListViewHolder(binding);

    }

    private CreditCardListViewHolder(ItemCreditCardBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public void bindTo(CreditCard card, CreditCardClickListener listener){

        mBinding.setCard(card);
        mBinding.setClickListener(listener);

    }
}
