package com.livefitter.theinsiders.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.theinsiders.adapter.viewholder.NotificationViewHolder;
import com.livefitter.theinsiders.listener.NotificationClickListener;
import com.livefitter.theinsiders.model.Notification;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 6/27/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationViewHolder>{


    private ArrayList<Notification> mList = new ArrayList<>();
    private NotificationClickListener mListener;
    private Context mContext;

    public NotificationAdapter(Context context, ArrayList<Notification> list, NotificationClickListener listener){

        mContext = context;
        mList = list;
        mListener = listener;

    }

    @Override
    public NotificationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return NotificationViewHolder.create(LayoutInflater.from(mContext), parent);
    }

    @Override
    public void onBindViewHolder(NotificationViewHolder holder, int position) {

        Notification notification = mList.get(position);

        holder.bindTo(notification, mListener);

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }
}
