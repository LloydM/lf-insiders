package com.livefitter.theinsiders.adapter;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;

import java.util.ArrayList;


/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class MyEventsPagerAdapter extends FragmentPagerAdapter {

    Context mContext;
    private ArrayList<BaseFragment> mFragmentArrayList;

    public MyEventsPagerAdapter(Context context, FragmentManager fm, ArrayList<BaseFragment> fragmentArrayList) {
        super(fm);
        this.mContext = context;
        this.mFragmentArrayList = fragmentArrayList;
    }

    @Override
    public Fragment getItem(int position) {
        return mFragmentArrayList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentArrayList != null? mFragmentArrayList.size() : 0;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if(position == 0){
            return mContext.getString(R.string.label_upcoming);
        }else if (position == 1){
            return mContext.getString(R.string.label_past);
        }
        else{
            return mContext.getString(R.string.label_interested);
        }
    }
}
