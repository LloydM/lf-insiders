package com.livefitter.theinsiders.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.livefitter.theinsiders.databinding.ItemNotificationBinding;
import com.livefitter.theinsiders.listener.NotificationClickListener;
import com.livefitter.theinsiders.model.Notification;

/**
 * Created by EJ Del Rosario
 * 27/06/2017
 * ejdelros08@gmail.com | ej.delrosario@fibo.ph
 * ------------------------
 * Copyright (c) Fibo
 * All Rights Reserved
 */

public class NotificationViewHolder extends RecyclerView.ViewHolder {

    private ItemNotificationBinding mBinding;

    public static NotificationViewHolder create (LayoutInflater inflater, ViewGroup parent){

        ItemNotificationBinding binding = ItemNotificationBinding.inflate(inflater, parent, false);

        return new NotificationViewHolder(binding);
    }

    private NotificationViewHolder(ItemNotificationBinding binding) {
        super(binding.getRoot());

        mBinding = binding;
    }

    public void bindTo(Notification notification, NotificationClickListener listener){

        mBinding.setNotification(notification);
        mBinding.setClickListener(listener);

    }
}
