package com.livefitter.theinsiders.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.livefitter.theinsiders.adapter.viewholder.CreditCardListViewHolder;
import com.livefitter.theinsiders.listener.CreditCardClickListener;
import com.livefitter.theinsiders.model.CreditCard;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 7/17/2017.
 */

public class CreditCardListAdapter extends RecyclerView.Adapter<CreditCardListViewHolder> {

    private ArrayList<CreditCard> creditCards;
    private Context mContext;
    private CreditCardClickListener mListener;

    public CreditCardListAdapter(Context context, ArrayList<CreditCard> creditCards, CreditCardClickListener listener){

        mContext = context;
        this.creditCards = creditCards;
        mListener = listener;

    }

    @Override
    public CreditCardListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return CreditCardListViewHolder.create(LayoutInflater.from(mContext), parent);
    }

    @Override
    public void onBindViewHolder(CreditCardListViewHolder holder, int position) {

        holder.bindTo(creditCards.get(position), mListener);

    }

    @Override
    public int getItemCount() {
        return creditCards == null ? 0 : creditCards.size();
    }
}
