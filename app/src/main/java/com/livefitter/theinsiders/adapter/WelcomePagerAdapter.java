package com.livefitter.theinsiders.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.util.Log;

import java.util.ArrayList;


/**
 * Created by LloydM on 2/6/17
 * for Livefitter
 */

public class WelcomePagerAdapter extends FragmentStatePagerAdapter {

    private ArrayList<Fragment> childFragments = new ArrayList<>();
    private ArrayList<String> titles = new ArrayList<>();

    public WelcomePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return childFragments.get(position);
    }

    @Override
    public int getCount() {
        return childFragments.size();
    }

    @Override
    public int getItemPosition(Object object) {
        // Return POSITION_NONE to force viewpager to refresh itself upon notifyDataSetChanged()
        return POSITION_NONE;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles.get(position);
    }

    public void replaceFragment(Fragment oldFragment, Fragment newFragment){
        int oldFragmentIndex = childFragments.indexOf(oldFragment);

        childFragments.set(oldFragmentIndex, newFragment);

        notifyDataSetChanged();

    }

    /**
     * should be called before setting this adapter to your viewpager
     * @param fragment
     * @param title
     */
    public void addFragment(Fragment fragment, String title){
        childFragments.add(fragment);
        titles.add(title);
    }
}
