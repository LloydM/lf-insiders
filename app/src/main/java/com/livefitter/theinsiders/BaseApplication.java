package com.livefitter.theinsiders;

import android.app.Application;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by LloydM on 3/14/17
 * for Livefitter
 */

public class BaseApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize our calligraphy config to set default font family used
        CalligraphyConfig config = new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Sk-Modernist-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build();
        CalligraphyConfig.initDefault(config);
    }
}
