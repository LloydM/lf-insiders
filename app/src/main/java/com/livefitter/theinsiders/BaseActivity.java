package com.livefitter.theinsiders;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by LloydM on 1/27/17
 * for Livefitter
 */

public class BaseActivity extends AppCompatActivity {

    public String getClassTag(){
        return this.getClass().getSimpleName();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    public void enableUpbutton(){
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    /**
     * Start another activity
     * @param cls Activity class
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Class<?> cls, boolean shouldFinish){

        Intent i = new Intent(this, cls);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        switchActivity(i, shouldFinish);
    }

    /**
     * Start another activity
     * @param cls Activity class
     * @param bundleKey Key for bundle extras
     * @param bundleExtras
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Class<?> cls, String bundleKey, Bundle bundleExtras, boolean shouldFinish){

        Intent i = new Intent(this, cls);
        i.putExtra(bundleKey, bundleExtras);

        switchActivity(i, shouldFinish);
    }

    /**
     * Start another activity
     * @param intent custom intent
     * @param shouldFinish Finish activity upon starting the next Activity
     */
    public void switchActivity(Intent intent, boolean shouldFinish){

        startActivity(intent);

        if(shouldFinish){
            finish();
        }
    }

    /**
     * Start another activity for result
     * @param intent
     * @param shouldFinish
     * @param requestCode
     */
    public void switchActivityForResult(Intent intent, boolean shouldFinish, int requestCode){

        startActivityForResult(intent, requestCode);

        if(shouldFinish){

            finish();

        }

    }

    /**
     * Replace the fragment in the given container
     * @param fragment The fragment to show
     * @param container Resource ID for the container
     * @param fragmentTag Fragment tag
     * @param addToBackStack Add this fragment to the backstack
     * @param clearBackStack Clear the backstack prior to adding the fragment
     */
    public void switchFragment(Fragment fragment, @IdRes int container, String fragmentTag, boolean addToBackStack, boolean clearBackStack){
        AppConstants.FRAG_CURRENT = fragmentTag;
        if(clearBackStack){
            getSupportFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if(addToBackStack){
            getSupportFragmentManager().beginTransaction().replace(container, fragment, fragmentTag).addToBackStack(fragmentTag).commit();
        }
        else {
            getSupportFragmentManager().beginTransaction().replace(container, fragment, fragmentTag).commit();
        }
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void hideSoftKeyboard(View view) {
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
}
