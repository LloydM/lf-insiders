package com.livefitter.theinsiders.request;

import android.content.Context;
import android.support.annotation.NonNull;

import com.google.firebase.iid.FirebaseInstanceId;
import com.livefitter.theinsiders.AppConstants;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LloydM on 2/13/17
 * for Livefitter
 */

public class LoginRequest extends BaseRequest {
    private JSONObject mParams;

    public LoginRequest(Context context, @NonNull JSONObject jsonParams, Callback requestCallback) {
        super(context, false, requestCallback);
        this.mParams = jsonParams;
    }

    @Override
    protected void buildRequest() {
        String url = AppConstants.urlLogIn();

        if(mParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, mParams.toString());

            mRequestBuilder.url(url)
                    .post(body);

            mRequest = mRequestBuilder.build();
        }
    }
}
