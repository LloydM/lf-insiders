package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LloydM on 2/16/17
 * for Livefitter
 */

public class LinkedInLogInRequest extends BaseRequest {

    private JSONObject jsonParams;

    public LinkedInLogInRequest(Context context, JSONObject jsonParams, Callback requestCallback) {
        super(context, false, requestCallback);

        this.jsonParams = jsonParams;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlLogInLinkedIn();

        if(jsonParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, jsonParams.toString());

            mRequestBuilder.url(url)
                    .post(body);

            mRequest = mRequestBuilder.build();
        }
    }
}
