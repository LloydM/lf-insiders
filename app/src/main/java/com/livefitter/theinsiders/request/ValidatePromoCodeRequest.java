package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class ValidatePromoCodeRequest extends BaseRequest {

    private String mPromoCode;
    private int mEventId;

    public ValidatePromoCodeRequest(Context context, String promoCode, int eventId, Callback requestCallback) {
        super(context, true, requestCallback);
        mPromoCode = promoCode;
        mEventId = eventId;
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlPromoCode() + "?promocode=" + mPromoCode +"&event_id=" + mEventId;

        mRequestBuilder.url(url).get();

        mRequest = mRequestBuilder.build();

    }
}
