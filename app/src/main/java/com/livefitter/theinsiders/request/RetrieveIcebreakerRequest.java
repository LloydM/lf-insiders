package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LloydM on 3/9/17
 * for Livefitter
 */

public class RetrieveIcebreakerRequest extends BaseRequest {
    public RetrieveIcebreakerRequest(Context context, Callback requestCallback) {
        super(context, true, requestCallback);
    }

    @Override
    protected void buildRequest() {
        String url = AppConstants.urlIcebreaker();

        RequestBody body = RequestBody.create(null, new byte[0]);

        mRequestBuilder.url(url)
        .post(body);

        mRequest = mRequestBuilder.build();
    }
}
