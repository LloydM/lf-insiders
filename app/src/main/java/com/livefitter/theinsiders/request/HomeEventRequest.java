package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LloydM on 2/1/17
 * for Livefitter
 */

public class HomeEventRequest extends BaseRequest {

    public HomeEventRequest(Context context, Callback requestCallback){
        super(context, true, requestCallback);
    }

    @Override
    protected void buildRequest() {
        String url = AppConstants.urlAllEvents();

        mRequestBuilder.url(url);

        mRequest = mRequestBuilder.build();
    }
}
