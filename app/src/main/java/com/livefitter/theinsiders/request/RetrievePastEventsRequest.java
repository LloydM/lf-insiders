package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * RSVP and guest invite requests are rolled into this one request as they use almost identical
 * Created by LloydM on 2/16/17
 * for Livefitter
 */

public class RetrievePastEventsRequest extends BaseRequest {

    public RetrievePastEventsRequest(Context context, Callback requestCallback) {
        super(context, true, requestCallback);
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlPastEvents();

        mRequestBuilder.url(url);

        mRequest = mRequestBuilder.build();
    }
}
