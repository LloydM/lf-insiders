package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class RetrieveOrderNumberRequest extends BaseRequest {

    public RetrieveOrderNumberRequest(Context context, Callback requestCallback) {
        super(context, true, requestCallback);
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlOrderNumber();

        mRequestBuilder.url(url).get();

        mRequest = mRequestBuilder.build();


    }
}
