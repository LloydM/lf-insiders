package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LloydM on 2/22/17
 * for Livefitter
 */

public class RetrieveUserProfileRequest extends BaseRequest {

    public RetrieveUserProfileRequest(Context context, Callback requestCallback) {
        super(context, true, requestCallback);
    }

    @Override
    protected void buildRequest() {
        String url = AppConstants.urlRetrieveUserProfile();

        mRequestBuilder.url(url);

        mRequest = mRequestBuilder.build();
    }
}
