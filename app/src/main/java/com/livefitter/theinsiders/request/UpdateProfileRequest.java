package com.livefitter.theinsiders.request;

import android.content.Context;
import android.support.annotation.NonNull;

import com.livefitter.theinsiders.AppConstants;

import org.json.JSONObject;

import okhttp3.Callback;
import okhttp3.RequestBody;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class UpdateProfileRequest extends BaseRequest {

    private JSONObject jsonParams;
    private int userId;

    public UpdateProfileRequest(Context context, @NonNull JSONObject jsonParams, int userId, Callback requestCallback) {
        super(context, true, requestCallback);

        this.jsonParams = jsonParams;
        this.userId = userId;

    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlUpdateProfile()+ "/" + userId;

        if(jsonParams != null) {
            RequestBody body = RequestBody.create(MEDIATYPE_JSON, jsonParams.toString());

            mRequestBuilder.url(url)
                    .put(body);

            mRequest = mRequestBuilder.build();
        }


    }
}
