package com.livefitter.theinsiders.request;

import android.content.Context;

import com.livefitter.theinsiders.AppConstants;

import okhttp3.Callback;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class FaqTermsRequest extends BaseRequest {

    public FaqTermsRequest(Context context, Callback requestCallback) {
        super(context, false, requestCallback);
    }

    @Override
    protected void buildRequest() {

        String url = AppConstants.urlFaqTerms();

        mRequestBuilder.url(url).get();

        mRequest = mRequestBuilder.build();


    }
}
