package com.livefitter.theinsiders.utility;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.linkedin.platform.AccessToken;
import com.livefitter.theinsiders.AppConstants;

/**
 * Created by LloydM on 2/13/17
 * for Livefitter
 */

public class SharedPreferencesHelper {

    /**
     * Determine if a user is logged in by checking the appropriate shared preference
     * @param context
     * @return if a user is logged in, based on the availability of the shared preference
     */
    public static boolean isUserLoggedIn(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);

        String authToken = sharedPreferences.getString(AppConstants.PREFS_KEY_USER_AUTH_TOKEN, "");

        return !authToken.isEmpty();
    }

    /**
     * Retrieve the authentication token for the currently logged in user
     * @param context
     * @return the authentication token
     */
    public static String getAuthToken(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);
        return sharedPreferences.getString(AppConstants.PREFS_KEY_USER_AUTH_TOKEN, "");
    }

    /**
     * Stores the authentication token to shared preferences
     * @param context
     * @param authToken
     */
    public static void saveAuthToken(Context context, String authToken){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.putString(AppConstants.PREFS_KEY_USER_AUTH_TOKEN, authToken)
                .apply();
    }

    /**
     * Check if the user has chosen their salutations
     * @param context
     * @return
     */
    public static boolean hasChosenSalutations(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);
        return sharedPreferences.getBoolean(AppConstants.PREFS_KEY_HAS_CHOSEN_SALUTATIONS, false);
    }

    /**
     * Set if the user has chosen their salutations
     * @param context
     */
    public static void saveHasChosenSalutations(Context context){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.putBoolean(AppConstants.PREFS_KEY_HAS_CHOSEN_SALUTATIONS, true)
                .apply();
    }

    /**
     * Removes the shared preference for the flag for the chosen salutations
     * @param context
     */
    public static void clearHasChosenSalutations(Context context){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.remove(AppConstants.PREFS_KEY_HAS_CHOSEN_SALUTATIONS).apply();
    }

    /**
     * Retrieve the linkedin authentication token to initialize a LinkedIn session instance with it
     * @param context
     */
    public static AccessToken getLiAuthToken(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);
        String liAuthToken = sharedPreferences.getString(AppConstants.PREFS_KEY_LI_AUTH_TOKEN, "");
        long liAuthTokenExpiry = sharedPreferences.getLong(AppConstants.PREFS_KEY_LI_AUTH_TOKEN_EXPIRY, 0);
        return new AccessToken(liAuthToken, liAuthTokenExpiry);
    }

    /**
     * Store the linkedin authentication token for later use.
     * This can, and will, expire so retrieve a new instance of it
     * @param context
     * @param liAuthToken
     */
    public static void saveLiAuthToken(Context context, String liAuthToken, long liAuthTokenExpiry){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.putString(AppConstants.PREFS_KEY_LI_AUTH_TOKEN, liAuthToken)
                .putLong(AppConstants.PREFS_KEY_LI_AUTH_TOKEN_EXPIRY, liAuthTokenExpiry)
                .apply();
    }

    /**
     * Removes the linkedin authentication token
     * @param context
     */
    public static void clearLiAuthToken(Context context){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.remove(AppConstants.PREFS_KEY_LI_AUTH_TOKEN)
                .remove(AppConstants.PREFS_KEY_LI_AUTH_TOKEN_EXPIRY)
                .apply();
    }

    /**
     * Determine if the user has seen the instructions for how to send invitations to guests
     * @param context
     */
    public static boolean knowsInviteInstructions(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);
        return  sharedPreferences.getBoolean(AppConstants.PREFS_KEY_KNOWS_INVITE_INSTRUCTIONS, false);
    }

    /**
     * Set the flag if the user has seen the instructions on sending invites
     */
    public static void setKnowsInviteInstructions(Context context){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.putBoolean(AppConstants.PREFS_KEY_KNOWS_INVITE_INSTRUCTIONS, true)
                .apply();
    }

    public static void setShouldDisplayWalkThrough(Context context, boolean shouldShow){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.putBoolean(AppConstants.PREFS_KEY_SHOULD_DISPLAY_WALKTHROUGH, shouldShow).apply();

    }

    public static boolean getShouldDisplayWalkThrough(Context context){

        SharedPreferences sharedPreferences = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE);

        return sharedPreferences.getBoolean(AppConstants.PREFS_KEY_SHOULD_DISPLAY_WALKTHROUGH, true);

    }

    /**
     * Removes the shared preference for the user data:
     * <ol>
     *     <li>User auth token</li>
     *     <li>Flag to display the walkthrough</li>
     *     <li>Flag to display the invitation instructions</li>
     *     <li>Flag to display the dialog for choosing the salutation</li>
     * </ol>
     * @param context
     */
    public static void clearUserData(Context context){

        SharedPreferences.Editor editor = context.getSharedPreferences(AppConstants.PREFS_MAIN, Context.MODE_PRIVATE).edit();

        editor.remove(AppConstants.PREFS_KEY_USER_AUTH_TOKEN)
                .remove(AppConstants.PREFS_KEY_SHOULD_DISPLAY_WALKTHROUGH)
                .remove(AppConstants.PREFS_KEY_KNOWS_INVITE_INSTRUCTIONS)
                .remove(AppConstants.PREFS_KEY_HAS_CHOSEN_SALUTATIONS)
                .apply();
    }
}
