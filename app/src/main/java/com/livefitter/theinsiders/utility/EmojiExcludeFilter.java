package com.livefitter.theinsiders.utility;

import android.text.InputFilter;
import android.text.Spanned;


/**
 * Taken from: https://stackoverflow.com/a/40533446
 * Created by LloydM on 11/28/17
 * for Livefitter
 */

public class EmojiExcludeFilter implements InputFilter {

    @Override
    public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
        for (int i = start; i < end; i++) {
            int type = Character.getType(source.charAt(i));
            if (type == Character.SURROGATE || type == Character.OTHER_SYMBOL) {
                return "";
            }
        }
        return null;
    }
}
