package com.livefitter.theinsiders.utility;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by LloydM on 2/2/17
 * for Livefitter
 */

public class DialogUtility {

    public static ProgressDialog showIndeterminateProgressDialog(@NonNull Context context, String title, @NonNull String message, boolean isCancellable){
        ProgressDialog dialog = new ProgressDialog(context);
        if(!title.isEmpty()) {
            dialog.setTitle(title);
        }

        dialog.setMessage(message);
        dialog.setCancelable(isCancellable);
        dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        dialog.setCanceledOnTouchOutside(isCancellable);
        dialog.show();

        return dialog;
    }
}
