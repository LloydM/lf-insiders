package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;

/**
 * Created by LFT-PC-010 on 7/5/2017.
 */

public class AddCreditCardActivity extends BaseActivity {

    private BaseFragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_credit_card);

        mFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_add_credit_card);

    }

    @Override
    public void onBackPressed() {
        if(mFragment == null) {
            super.onBackPressed();
        }else if(mFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }

}
