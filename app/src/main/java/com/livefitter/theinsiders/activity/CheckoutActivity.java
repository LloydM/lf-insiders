package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.fragment.PriceSummaryFragment;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.Guest;

import java.util.ArrayList;

/**
 * Created by LFT-PC-010 on 7/14/2017.
 */

public class CheckoutActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        initialize();

    }

    private void initialize() {

        EventModel mEventModel = getIntent().getParcelableExtra(AppConstants.KEY_EVENT_MODEL);
        ArrayList<Guest> invitedGuests = getIntent().getParcelableArrayListExtra(AppConstants.KEY_INVITED_GUEST_LIST);

        Bundle bundle = new Bundle();

        bundle.putParcelable(AppConstants.KEY_EVENT_MODEL, mEventModel);
        bundle.putParcelableArrayList(AppConstants.KEY_INVITED_GUEST_LIST, invitedGuests);

        PriceSummaryFragment fragment = new PriceSummaryFragment();
        fragment.setArguments(bundle);

        switchFragment(fragment, R.id.checkout_fragment_container, AppConstants.FRAG_PRICE_SUMMARY, true, true);

    }

    @Override
    public void onBackPressed() {

        BaseFragment currentFragment = (BaseFragment) getSupportFragmentManager().findFragmentByTag(AppConstants.FRAG_CURRENT);

        if(currentFragment != null && currentFragment.isVisible()){

            if(currentFragment.onBackPressed()){

                super.onBackPressed();

            }

        }
        else {

            super.onBackPressed();

        }
    }
}
