package com.livefitter.theinsiders.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.fragment.HomeFragment;
import com.livefitter.theinsiders.fragment.IcebreakerStartFragment;
import com.livefitter.theinsiders.fragment.MyEventsFragment;
import com.livefitter.theinsiders.fragment.NotificationFragment;
import com.livefitter.theinsiders.fragment.ProfileFragment;
import com.livefitter.theinsiders.fragment.SupportFragment;
import com.livefitter.theinsiders.model.UserProfile;
import com.livefitter.theinsiders.request.LogOutRequest;
import com.livefitter.theinsiders.request.RetrieveUserProfileRequest;
import com.livefitter.theinsiders.request.UpdateSalutationRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;
import com.livefitter.theinsiders.view.SalutationDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        SalutationDialogFragment.OnSalutationSelectedListener,
        View.OnClickListener {

    private final int mFragmentContainerResId = R.id.fragment_container;

    private BaseFragment fragCurrent;
    private HomeFragment fragHome;
    private MyEventsFragment fragMyEvents;
    private IcebreakerStartFragment fragIcebreakerStart;
    private ProfileFragment fragProfile;
    private SupportFragment fragSupport;
    private NotificationFragment fragNotification;

    private DrawerLayout mDrawerLayout;
    private ProgressDialog dProgress;
    private SalutationDialogFragment dSalutations;
    private NavigationView mNavigationView;
    private View vNavigationHeaderView;
    private TextView tvUserName, tvUserEmail;

    private UserProfile mUserProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationView = (NavigationView) findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        vNavigationHeaderView = mNavigationView.getHeaderView(0);
        tvUserName = (TextView) vNavigationHeaderView.findViewById(R.id.nav_header_user_name);
        tvUserEmail = (TextView) vNavigationHeaderView.findViewById(R.id.nav_header_user_email);
        vNavigationHeaderView.setOnClickListener(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(AppConstants.KEY_USER_PROFILE)) {
            mUserProfile = extras.getParcelable(AppConstants.KEY_USER_PROFILE);
        }

        if(mUserProfile != null){
            setUpHomeScreen();
            showSalutationDialog();
        }else{
            retrieveUserProfile();
        }
    }

    /**
     * Allows child fragments of this activity to set up the drawerlayout with their own supplied mToolbar
     * @param toolbar
     */
    public void setupDrawerWithToolbar(Toolbar toolbar){
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawerLayout.addDrawerListener(toggle);
        toggle.syncState();
    }

    @Override
    public void onBackPressed() {

        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){

            mDrawerLayout.closeDrawer(GravityCompat.START);
            return;
        }

        // Let the child fragments determine if it is safe to let this activity handle the back functionality
        if(fragCurrent != null && !fragCurrent.onBackPressed()){
            return;
        }

        if(getSupportFragmentManager().getBackStackEntryCount() == 1){
            finish();
        }else{
            super.onBackPressed();
            if(getSupportFragmentManager().getBackStackEntryCount() == 1){
                mNavigationView.setCheckedItem(R.id.nav_home);
            }
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        mDrawerLayout.closeDrawer(GravityCompat.START);

        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            return navigateToChild(AppConstants.FRAG_HOME);
        } else if (id == R.id.nav_my_events) {
            return navigateToChild(AppConstants.FRAG_MY_EVENTS);
        } else if (id == R.id.nav_icebreaker) {
            return navigateToChild(AppConstants.FRAG_ICEBREAKER);
        } else if (id == R.id.nav_profile) {
            return navigateToChild(AppConstants.FRAG_PROFILE);
        } else if (id == R.id.nav_support) {
            return navigateToChild(AppConstants.FRAG_SUPPORT);
        } else if (id == R.id.nav_notifications){
            return navigateToChild(AppConstants.FRAG_NOTIFICATION);
        }
        else if (id == R.id.nav_sign_out) {
            showSignOutDialog();
        }
        return false;
    }

    private boolean navigateToChild(String fragmentTag){
        BaseFragment fragment = null;
        if (fragmentTag.equals(AppConstants.FRAG_HOME)) {
            fragment = fragHome;
        } else if (fragmentTag.equals(AppConstants.FRAG_MY_EVENTS)) {
            if(fragMyEvents == null){
                fragMyEvents = new MyEventsFragment();
            }

            fragment = fragMyEvents;
        } else if (fragmentTag.equals(AppConstants.FRAG_ICEBREAKER)) {

            if(fragIcebreakerStart == null){
                fragIcebreakerStart = new IcebreakerStartFragment();
            }

            fragment = fragIcebreakerStart;
        } else if (fragmentTag.equals(AppConstants.FRAG_PROFILE)) {

            if(fragProfile == null){
                fragProfile = new ProfileFragment();

                if(mUserProfile != null){
                    Bundle arguments = new Bundle();
                    arguments.putParcelable(AppConstants.KEY_USER_PROFILE, mUserProfile);
                    fragProfile.setArguments(arguments);
                }
            }

            fragment = fragProfile;

        }
        else if(fragmentTag.equals(AppConstants.FRAG_SUPPORT)){

            if(fragSupport == null){
                fragSupport = new SupportFragment();
            }

            fragment = fragSupport;

        }
        else if(fragmentTag.equals(AppConstants.FRAG_NOTIFICATION)){

            if(fragNotification == null){
                fragNotification = new NotificationFragment();
            }

            fragment = fragNotification;

        }

        if (!fragmentTag.isEmpty() && !fragmentTag.equals(AppConstants.FRAG_CURRENT)) {
            boolean isHomeFragment = fragmentTag.equals(AppConstants.FRAG_HOME);

            changeFragments(fragment, fragmentTag, isHomeFragment);

            return true;
        }

        return false;
    }

    private void changeFragments(BaseFragment fragment, String fragmentTag, boolean isHomeFragment){

        // Clear backstack except for the HomeFragment
        getSupportFragmentManager().popBackStackImmediate(AppConstants.FRAG_HOME, 0);

        if(!isHomeFragment){
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            transaction.replace(mFragmentContainerResId, fragment, fragmentTag);
            transaction.addToBackStack(fragmentTag);
            transaction.commit();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == vNavigationHeaderView){
            if(navigateToChild(AppConstants.FRAG_PROFILE)){
                mNavigationView.setCheckedItem(R.id.nav_profile);
                mDrawerLayout.closeDrawer(GravityCompat.START);
            }
        }
    }

    private void setUpHomeScreen(){
        fragCurrent = fragHome = new HomeFragment();

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(mFragmentContainerResId, fragCurrent, AppConstants.FRAG_HOME);
        transaction.addToBackStack(AppConstants.FRAG_HOME);
        transaction.commit();

        mNavigationView.setCheckedItem(R.id.nav_home);

        setNavigationHeaderValues();

        if(mUserProfile.getSignInCount() <= 1 && SharedPreferencesHelper.getShouldDisplayWalkThrough(this)) {
            switchActivity(WalkthroughActivity.class, false);
        }
    }

    private void setNavigationHeaderValues(){

        if(mUserProfile.getName() != null && !mUserProfile.getName().isEmpty()){
            tvUserName.setText(mUserProfile.getName());
        }

        if(mUserProfile.getEmail() != null && !mUserProfile.getEmail().isEmpty()){
            tvUserEmail.setText(mUserProfile.getEmail());
        }

    }

    /**
     * Retrieve the user's profile
     *
     * This is called when we're returning to this when the user has already logged in.
     */
    private void retrieveUserProfile() {
        dProgress = DialogUtility.showIndeterminateProgressDialog(this,
                "",
                getString(R.string.label_retrieving_profile),
                false);

        RetrieveUserProfileRequest retrieveUserProfileRequest = new RetrieveUserProfileRequest(this, new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("~~", "onResponse response: " + response);

                if(dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if(response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d("~~", "onResponse responseBody: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if(responseBodyJson.has("status")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                if(statusOK) {
                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new GsonBuilder().serializeNulls().create();
                                    Type typeToken = new TypeToken<UserProfile>(){}.getType();
                                    UserProfile userProfile = gson.fromJson(dataResponse, typeToken);

                                    if(userProfile != null) {
                                        Log.d("~~", "onResponse: userProfile: " + userProfile);
                                        mUserProfile = userProfile;

                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                setUpHomeScreen();
                                                showSalutationDialog();
                                            }
                                        });
                                    }

                                }else{
                                    final String messageResponse = responseBodyJson.getString("message");

                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {

                                            String message = messageResponse;
                                            if(message.isEmpty()){

                                                message = getString(R.string.error_retrieve_profile);
                                            }

                                            DialogUtil.showAlertDialog(MainActivity.this,
                                                    getString(R.string.label_error),
                                                    message,
                                                    new DialogInterface.OnDismissListener() {
                                                        @Override
                                                        public void onDismiss(DialogInterface dialogInterface) {
                                                            signOutUser();
                                                        }
                                                    });
                                        }
                                    });
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    DialogUtil.showAlertDialog(MainActivity.this,
                                            getString(R.string.label_error),
                                            getString(R.string.error_retrieve_profile),
                                            new DialogInterface.OnDismissListener() {
                                                @Override
                                                public void onDismiss(DialogInterface dialogInterface) {
                                                    signOutUser();
                                                }
                                            });
                                }
                            });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getClassTag(), "onFailure exception " + e.getMessage());

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(MainActivity.this,
                                getString(R.string.label_error),
                                getString(R.string.error_retrieve_profile),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        signOutUser();
                                    }
                                });
                    }
                });
            }
        });

        retrieveUserProfileRequest.execute();
    }

    /**
     * Determine if the current user has already selected their salutations,
     * otherwise prompt them to select it now.
     */
    public void showSalutationDialog() {

        boolean userProfileHasSalutations = mUserProfile != null
                && mUserProfile.getSalutation() != null
                && !mUserProfile.getSalutation().isEmpty();
        boolean hasChosenSalutations = userProfileHasSalutations || SharedPreferencesHelper.hasChosenSalutations(this);

        if(!hasChosenSalutations){
            dSalutations = new SalutationDialogFragment();
            dSalutations.setListener(this);
            dSalutations.show(getSupportFragmentManager(), "salutation");
        } else {
            SharedPreferencesHelper.saveHasChosenSalutations(this);
        }
    }

    @Override
    public void onSalutationSelected(String salutation) {
        dSalutations.dismiss();

        // Call the salutation update request
        if(mUserProfile != null && !salutation.isEmpty()) {

            //update gender according to selected salutation even before sending to backend
            //to ensure gender reflection on profile activity right away
            if(salutation.equals(getString(R.string.salutations_mister))){

                mUserProfile.setGender("Male");

            }
            else{

                mUserProfile.setGender("Female");

            }

            UpdateSalutationRequest mSalutationRequest = new UpdateSalutationRequest(this, mUserProfile.getId(), salutation, new Callback() {

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    Log.d("~~", "onResponse response: " + response);

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    if (response.isSuccessful()) {
                        String responseBody = response.body().string();
                        if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                            Log.d("~~", "onResponse responseBody: " + responseBody);

                            try {
                                JSONObject responseBodyJson = new JSONObject(responseBody);

                                if (responseBodyJson.has("status")) {
                                    boolean statusOK = responseBodyJson.getBoolean("status");
                                    final String messageResponse = responseBodyJson.getString("message");

                                    // We do not show the confirmation message
                                    /*if (!messageResponse.isEmpty()) {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                DialogUtil.showAlertDialog(MainActivity.this, messageResponse);
                                            }
                                        });
                                    }*/

                                    if(statusOK){
                                        // Store the salutations flag in the shared preferences
                                        SharedPreferencesHelper.saveHasChosenSalutations(MainActivity.this);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        DialogUtil.showAlertDialog(MainActivity.this,
                                                getString(R.string.label_error),
                                                getString(R.string.error_generic_error));
                                    }
                                });
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call call, IOException e) {

                    Log.d(getClassTag(), "onFailure exception " + e.getMessage());

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (dProgress != null && dProgress.isShowing()) {
                                dProgress.dismiss();
                            }

                            DialogUtil.showAlertDialog(MainActivity.this,
                                    getString(R.string.label_error),
                                    getString(R.string.error_generic_error));
                        }
                    });
                }
            });

            mSalutationRequest.execute();
        }
    }

    private void showSignOutDialog() {
        DialogUtil.showConfirmationDialog(this,
                getString(R.string.label_sign_out),
                getString(R.string.msg_confirm_sign_out),
                getString(android.R.string.yes),
                getString(android.R.string.no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        if (which == Dialog.BUTTON_POSITIVE) {
                            signOutUser();
                        }
                    }
                });
    }

    private void signOutUser() {

        if(mUserProfile != null){

            dProgress = DialogUtility.showIndeterminateProgressDialog(this,
                    getString(R.string.label_sign_out),
                    getString(R.string.label_signing_out_account),
                    false);

            LogOutRequest logOutRequest = new LogOutRequest(this, mUserProfile.getId(), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {


                    Log.d(MainActivity.this.getClassTag(), "onFailure exception " + e.getMessage());

                    MainActivity.this.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (dProgress != null && dProgress.isShowing()) {
                                dProgress.dismiss();
                            }

                            DialogUtil.showAlertDialog(MainActivity.this,
                                    getString(R.string.label_error),
                                    getString(R.string.error_generic_error));
                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if(dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    if(response.isSuccessful()) {
                        String responseBody = response.body().string();
                        if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                            Log.d(getClassTag(), "onResponse body: " + responseBody);

                            try {
                                JSONObject responseBodyJson = new JSONObject(responseBody);

                                if(responseBodyJson.has("status")) {
                                    boolean statusOK = responseBodyJson.getBoolean("status");

                                    if(statusOK){

                                        MainActivity.this.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                SharedPreferencesHelper.clearUserData(MainActivity.this);
                                                SharedPreferencesHelper.clearLiAuthToken(MainActivity.this);
                                                switchActivity(WelcomeActivity.class, true);
                                            }
                                        });
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();

                                MainActivity.this.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        DialogUtil.showAlertDialog(MainActivity.this,
                                                getString(R.string.label_error),
                                                getString(R.string.error_generic_error));
                                    }
                                });
                            }
                        }
                    }
                }
            });

            logOutRequest.execute();
        }else{
            // If the user profile is invalid, log out of the system without notifying the server

            SharedPreferencesHelper.clearUserData(MainActivity.this);

            switchActivity(WelcomeActivity.class, true);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.i(getClassTag(), "onActivityResult");

        if(resultCode == RESULT_OK){

            Log.i(getClassTag(), "result ok");

            switch (requestCode){

                case AppConstants.REQUEST_CODE_UPDATE_PROFILE:{

                    Log.i(getClassTag(), "update profile code");

                    if(fragProfile != null && fragProfile.isAdded()){

                        Log.i(getClassTag(), "fragment not null");

                        mUserProfile = data.getParcelableExtra(AppConstants.KEY_USER_PROFILE);
                        setNavigationHeaderValues();

                        fragProfile.updateProfile(mUserProfile);

                    }

                    break;
                }
            }
        }
    }
}
