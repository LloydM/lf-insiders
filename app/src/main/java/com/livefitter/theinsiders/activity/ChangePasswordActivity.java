package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;


/**
 * Created by LloydM on 2/22/17
 * for Livefitter
 */

public class ChangePasswordActivity extends BaseActivity {

    BaseFragment fragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        fragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_change_password);
    }

    @Override
    public void onBackPressed() {
        if(fragment == null) {
            super.onBackPressed();
        }else if(fragment.onBackPressed()) {
            super.onBackPressed();
        }
    }
}
