package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;

/**
 * Created by LFT-PC-010 on 6/20/2017.
 */

public class CrowdSourcedEventDetailActivity extends BaseActivity {

    BaseFragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crowd_sourced_event_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_crowd_sourced_event_detail);
    }

    @Override
    public void onBackPressed() {
        if(mFragment == null) {
            super.onBackPressed();
        }else if(mFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }
}
