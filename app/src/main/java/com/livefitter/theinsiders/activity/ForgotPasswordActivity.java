package com.livefitter.theinsiders.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.request.ForgotPasswordRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 6/29/2017.
 */

public class ForgotPasswordActivity extends BaseActivity {

    private Toolbar toolbar;
    private TextInputLayout tilEmail;
    private TextInputEditText etEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initialize();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        colorizeHomeButton();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }

        }
    }

    protected void colorizeHomeButton(){

        // Cheat way to colorize the hamburger icon without tweaking the colorPrimary etc
        // Color is based on "gold" color in color res
        int color = Color.parseColor("#dec666");
        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP);

        for (int i = 0; i < toolbar.getChildCount(); i++) {
            final View v = toolbar.getChildAt(i);

            if (v instanceof ImageButton) {
                ((ImageButton) v).setColorFilter(colorFilter);
            }
        }
    }

    private void initialize() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tilEmail = (TextInputLayout) findViewById(R.id.edittext_layout_email);
        etEmail = (TextInputEditText) findViewById(R.id.edittext_email);

        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));

        findViewById(R.id.forgot_password_button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                resetPassword();

            }
        });

    }

    private void resetPassword() {

        if(isValid()){

            final ProgressDialog pdLoading = DialogUtility.showIndeterminateProgressDialog(this,
                    getString(R.string.title_forgot_password),
                    getString(R.string.label_resetting_password),
                    false);

            try{

                JSONObject param = createJsonParams();

                ForgotPasswordRequest request = new ForgotPasswordRequest(this, param, new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        if(pdLoading != null && pdLoading.isShowing()){
                            pdLoading.dismiss();
                        }

                        DialogUtil.showAlertDialog(ForgotPasswordActivity.this,
                                getString(R.string.title_forgot_password),
                                getString(R.string.error_generic_error));
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        if(pdLoading != null && pdLoading.isShowing()){
                            pdLoading.dismiss();
                        }

                        String errorMessage = null;

                        try {

                            JSONObject jobj = new JSONObject(response.body().string());

                            boolean statusOK = jobj.getBoolean("status");

                            if(statusOK){

                                final String successMessage = jobj.getString("message");

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        DialogUtil.showAlertDialog(ForgotPasswordActivity.this,
                                                successMessage,
                                                new DialogInterface.OnDismissListener() {
                                                    @Override
                                                    public void onDismiss(DialogInterface dialog) {
                                                        onBackPressed();
                                                    }
                                                });

                                    }
                                });
                            }
                            else{

                                errorMessage = jobj.getString("message");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            errorMessage = getString(R.string.error_generic_error);
                        }


                        if(errorMessage != null){

                            final String finalErrorMessage = errorMessage;

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    DialogUtil.showAlertDialog(ForgotPasswordActivity.this,
                                            finalErrorMessage);

                                }
                            });

                        }

                    }
                });

                request.execute();

            } catch (JSONException e) {
                e.printStackTrace();

                if(pdLoading != null && pdLoading.isShowing()){
                    pdLoading.dismiss();
                }

                DialogUtil.showAlertDialog(this,
                        getString(R.string.title_forgot_password),
                        getString(R.string.error_generic_error));
            }

        }

    }

    private boolean isValid(){

        boolean isValid = true;

        if (etEmail.getText().toString().trim().length() == 0) {
            String fieldName = getString(R.string.label_email);
            String requiredFieldErrorMessage = getString(R.string.error_required_field, fieldName);
            tilEmail.setError(requiredFieldErrorMessage);
            isValid = false;
        }
        else if(!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText()).matches()){
            String invalidEmailErrorMessage = getString(R.string.error_invalid_email);
            tilEmail.setError(invalidEmailErrorMessage);
            isValid = false;
        }

        return isValid;

    }

    private JSONObject createJsonParams() throws JSONException {

        JSONObject jobj = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("email", etEmail.getText().toString());

        jobj.put("data", jobjData);

        return jobj;

    }
}
