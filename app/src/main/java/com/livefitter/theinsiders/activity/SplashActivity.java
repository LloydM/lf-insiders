package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.os.PersistableBundle;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;


/**
 * Created by LloydM on 2/6/17
 * for Livefitter
 */

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(SharedPreferencesHelper.isUserLoggedIn(this)){
            switchActivity(MainActivity.class, true);
        }else{
            switchActivity(WelcomeActivity.class, true);
        }
    }
}
