package com.livefitter.theinsiders.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.MenuItem;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class UpdateProfileActivity extends BaseActivity {

    private BaseFragment mFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mFragment = (BaseFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_update_profile);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:{
                onBackPressed();
                return true;
            }
            default:{
                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if(mFragment == null) {
            super.onBackPressed();
        }else if(mFragment.onBackPressed()) {
            super.onBackPressed();
        }
    }
}
