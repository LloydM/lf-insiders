package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.databinding.FragmentCrowdSourcedEventDetailConstraintBinding;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.request.ExpressInterestRequest;
import com.livefitter.theinsiders.request.RemoveInterestRequest;
import com.livefitter.theinsiders.request.RetrieveEventDetailRequest;
import com.livefitter.theinsiders.utility.DialogUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/1/17
 * for Livefitter
 */

public class CrowdSourcedEventDetailConstraintController extends BaseController implements View.OnClickListener {

    private FragmentCrowdSourcedEventDetailConstraintBinding mBinding;

    private FrameLayout flBlocker;
    private Button btnExpressInterest;
    private ProgressDialog dProgress;

    private EventModel mEvent;

    /**
     * Flag that is set when we need to retrieve the entire Event details
     */
    private boolean shouldRetrieveEventDetails = true;

    /**
     * Flag that is set if this event is expired.
     * This is only set when we're coming from the PastEvents screen
     */
    private boolean isEventExpired;

    public CrowdSourcedEventDetailConstraintController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);

        Bundle extras = getActivity().getIntent().getExtras();
        if (extras != null) {
            if (extras.containsKey(AppConstants.KEY_EVENT_MODEL)) {
                mEvent = extras.getParcelable(AppConstants.KEY_EVENT_MODEL);

                isEventExpired = extras.getBoolean(AppConstants.KEY_EVENT_EXPIRED, false);
            }
        }

    }

    public void retrieveFullEventDetails() {

        if (!shouldRetrieveEventDetails) {
            return;
        }

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_retrieving_event_detail),
                false);

        RetrieveEventDetailRequest request = new RetrieveEventDetailRequest(getActivity(), mEvent.getId(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status") && responseBodyJson.has("data")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                if (statusOK) {

                                    shouldRetrieveEventDetails = false;

                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new Gson();
                                    Type typeToken = new TypeToken<EventModel>() {
                                    }.getType();
                                    EventModel eventModel = gson.fromJson(dataResponse, typeToken);

                                    if (eventModel != null) {
                                        mEvent.setAllFields(eventModel);

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                flBlocker.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }
                                else {

                                    String errorMessage = responseBodyJson.getString("message");

                                    showThreadSafeAlertDialog(
                                            getActivity().getString(R.string.label_error),
                                            errorMessage,
                                            new DialogInterface.OnDismissListener() {
                                                @Override
                                                public void onDismiss(DialogInterface dialogInterface) {
                                                    getActivity().finish();
                                                }
                                            });

                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            getActivity().finish();
                                        }
                                    });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        getActivity().finish();
                                    }
                                });

                    }
                });
            }
        });

        request.execute();
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().enableUpbutton();
        getFragment().setHasOptionsMenu(true);

        flBlocker = (FrameLayout) rootView.findViewById(R.id.loading_blocker);
        btnExpressInterest = (Button) rootView.findViewById(R.id.event_detail_express_interest_button);

        btnExpressInterest.setOnClickListener(this);

        if (mEvent != null) {
            mBinding.setEventModel(mEvent);
        }
    }

    public boolean onBackPressed() {
        return true;
    }

    @Override
    public void onClick(View view) {

        if (view == btnExpressInterest) {
            if(mEvent.getHasExpressedInterest()){

                removeInterest();

            }
            else{

                expressInterest();

            }
        }
    }

    private void expressInterest(){

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getActivity().getString(R.string.label_loading),
                false);

        try {

            JSONObject jobjParams = createJsonParams();

            ExpressInterestRequest request = new ExpressInterestRequest(getActivity(), jobjParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    DialogUtil.showAlertDialog(getActivity(),
                            getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    String errorMessage = null;

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean status = jobj.getBoolean("status");

                        if(!status){
                            errorMessage = jobj.getString("message");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        errorMessage = getActivity().getString(R.string.error_generic_error);
                    }

                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                errorMessage);

                    }
                    else{
                        mEvent.setHasExpressedInterest(true);

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.msg_interest_expressed), new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                        getActivity().onBackPressed();

                                    }
                                });

                            }
                        });

                    }

                }
            });


            request.execute();

        } catch (JSONException e) {
            e.printStackTrace();

            if (dProgress != null && dProgress.isShowing()) {
                dProgress.dismiss();
            }

            DialogUtil.showAlertDialog(getActivity(),
                    getActivity().getString(R.string.label_error),
                    getActivity().getString(R.string.error_generic_error));
        }

    }

    private JSONObject createJsonParams() throws JSONException {

        JSONObject jobjParams = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("event_id", mEvent.getId());

        jobjParams.put("data", jobjData);

        return jobjParams;
    }

    private void removeInterest(){

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getActivity().getString(R.string.label_loading),
                false);

        RemoveInterestRequest request = new RemoveInterestRequest(getActivity(), mEvent.getId(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                String errorMessage = null;

                try {

                    JSONObject jobj = new JSONObject(response.body().string());

                    boolean status = jobj.getBoolean("status");

                    if(!status){
                        errorMessage = jobj.getString("message");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    errorMessage = getActivity().getString(R.string.error_generic_error);
                }

                if (errorMessage != null){

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            errorMessage);

                }
                else{

                    mEvent.setHasExpressedInterest(false);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.msg_interest_removed), new DialogInterface.OnDismissListener() {
                                @Override
                                public void onDismiss(DialogInterface dialog) {

                                    getActivity().onBackPressed();

                                }
                            });

                        }
                    });

                }

            }
        });

        request.execute();

    }


    public void setBinding(FragmentCrowdSourcedEventDetailConstraintBinding mBinding) {
        this.mBinding = mBinding;
    }

    @Override
    public void onStop() {
        if (dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
