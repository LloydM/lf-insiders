package com.livefitter.theinsiders.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.request.SaveCardRequest;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;
import com.livefitter.theinsiders.view.ExpirationDateDialogFragment;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.android.view.CardInputWidget;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import ejdelrosario.framework.utilities.DateUtil;
import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by LFT-PC-010 on 7/5/2017.
 */

public class AddCreditCardController extends BaseController /**implements ExpirationDateDialogFragment.OnExpiryDateSelectListener**/ {

//    private TextInputLayout tilCardNumber, tilExpirationDate, tilCvc;
//    private TextInputEditText etCardNumber, etExpirationDate, etCvc;
    private CardInputWidget mCardInputWidget;
    private AppCompatCheckBox cbSetDefault;
    private ProgressDialog pdLoading;
    private boolean isCheckOutMode;
//    private Calendar mCalendar = Calendar.getInstance();

    public AddCreditCardController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        if(getFragment().getArguments() != null) {

            isCheckOutMode = getFragment().getArguments().getBoolean(AppConstants.KEY_CHECK_OUT_MODE, false);

        }
        else{

            isCheckOutMode = false;

        }

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getActivity().getSupportActionBar().setTitle(isCheckOutMode ? getActivity().getString(R.string.label_check_out) : getActivity().getString(R.string.label_payment_details));

//        tilCardNumber       = (TextInputLayout) rootView.findViewById(R.id.check_out_edittext_card_number_layout);
//        tilExpirationDate   = (TextInputLayout) rootView.findViewById(R.id.check_out_edittext_expiration_date_layout);
//        tilCvc              = (TextInputLayout) rootView.findViewById(R.id.check_out_edittext_cvc_layout);
//
//        etCardNumber        = (TextInputEditText) rootView.findViewById(R.id.check_out_edittext_card_number);
//        etExpirationDate    = (TextInputEditText) rootView.findViewById(R.id.check_out_edittext_expiration_date);
//        etCvc               = (TextInputEditText) rootView.findViewById(R.id.check_out_edittext_cvc);

        mCardInputWidget    = (CardInputWidget) rootView.findViewById(R.id.check_out_card_input_widget);

        cbSetDefault        = (AppCompatCheckBox) rootView.findViewById(R.id.check_out_checkbox_default);

//        etCardNumber.addTextChangedListener(new FormFieldTextWatcher(tilCardNumber));
//        etExpirationDate.addTextChangedListener(new FormFieldTextWatcher(tilExpirationDate));
//        etCvc.addTextChangedListener(new FormFieldTextWatcher(tilCvc));
//
//        etExpirationDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View view, boolean hasFocus) {
//
//                if(hasFocus){
//
//                    InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
//                    inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
//
//                    showExpiryDatePicker();
//
//                }
//
//            }
//        });

//        etExpirationDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                showExpiryDatePicker();
//
//            }
//        });

        rootView.findViewById(R.id.check_out_button_submit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                validateCard();

            }
        });

    }

    @Override
    public void onStop() {

    }

//    private void showExpiryDatePicker(){
//
//        ExpirationDateDialogFragment.Create(mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.YEAR), this).show(getActivity().getSupportFragmentManager(), null);
//
//    }

//    @Override
//    public void onDateSelect(int month, int year) {
//
//        mCalendar.set(Calendar.MONTH, month);
//        mCalendar.set(Calendar.YEAR, year);
//
//        etExpirationDate.setText(DateUtil.getDate(AppConstants.DATE_STRIPE_FORMAT, mCalendar.getTime()));
//
//    }

//    private void validateCard(){
//
//        String cardNumber = etCardNumber.getText().toString();
//        int expMonth = mCalendar.get(Calendar.MONTH);
//        int expYear = mCalendar.get(Calendar.YEAR);
//        String cvc = etCvc.getText().toString();
//
//        boolean isValid = true;
//
//        Card card = new Card(cardNumber, expMonth, expYear, cvc);
//
//        if(!card.validateNumber()){
//
//            String fieldName = getActivity().getString(R.string.label_credit_card_number);
//            String errorMessage = getActivity().getString(R.string.error_field_invalid, fieldName);
//            tilCardNumber.setError(errorMessage);
//            isValid = false;
//
//        }
//
//        if(etExpirationDate.length() <= 0 || !card.validateExpiryDate()){
//
//            String fieldName = getActivity().getString(R.string.label_expiration_date);
//            String errorMessage = getActivity().getString(R.string.error_field_invalid, fieldName);
//            tilExpirationDate.setError(errorMessage);
//            isValid = false;
//
//        }
//
//        if(!card.validateCVC()){
//
//            String fieldName = getActivity().getString(R.string.label_cvc);
//            String errorMessage = getActivity().getString(R.string.error_field_invalid, fieldName);
//            tilCvc.setError(errorMessage);
//            isValid = false;
//
//        }
//
//        if(isValid){
//
//            tokenizeCard(card);
//
//        }
//
//    }

    private void validateCard(){

        Card card = mCardInputWidget.getCard();

        if(card  != null){

            tokenizeCard(card);

        }

    }

    private void showProgressDialog(String message){

        if(pdLoading == null) {
            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);
        }

        pdLoading.setMessage(message);

        if(!pdLoading.isShowing()){

            pdLoading.show();

        }

    }

    private void hideProgressDialog(){

        if(pdLoading != null && pdLoading.isShowing()){

            pdLoading.dismiss();

        }

    }

    private void tokenizeCard(Card card){

        showProgressDialog(getActivity().getString(R.string.label_validating));

        Stripe stripe = new Stripe(getActivity(), AppConstants.getStripeApiKey());

        stripe.createToken(card, new TokenCallback() {
            @Override
            public void onError(Exception error) {

                hideProgressDialog();
                DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.error_invalid_card));

            }

            @Override
            public void onSuccess(Token token) {

                saveCardToken(token.getId());

            }
        });

    }

    private void saveCardToken(String cardTokenId){

        showProgressDialog(getActivity().getString(R.string.label_loading));

        try {

            JSONObject jobjParams = createJsonParams(cardTokenId);

            SaveCardRequest request = new SaveCardRequest(getActivity(), jobjParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    hideProgressDialog();

                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));

                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    hideProgressDialog();

                    String errorMessage = null;

                    if(response.isSuccessful()) {

                        try {

                            JSONObject jobj = new JSONObject(response.body().string());

                            boolean statusOK = jobj.getBoolean("status");

                            if(!statusOK){

                                errorMessage = jobj.getString("message");

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            errorMessage = getActivity().getString(R.string.error_generic_error);
                        }

                    }
                    else{

                        errorMessage = getActivity().getString(R.string.error_generic_error);

                    }

                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error), errorMessage);

                    }
                    else{

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.msg_card_added_successfully), new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialog) {

                                        getActivity().onBackPressed();

                                    }
                                });

                            }
                        });

                    }

                }
            });

            request.execute();

        } catch (JSONException e) {
            e.printStackTrace();

            hideProgressDialog();
            DialogUtil.showAlertDialog(getActivity(), getActivity().getString(R.string.error_generic_error));
        }

    }

    private JSONObject createJsonParams(String cardTokenId) throws JSONException {

        JSONObject jobj = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("is_default", cbSetDefault.isChecked());
        jobjData.put("stripe_token", cardTokenId);

        jobj.put("data", jobjData);

        return jobj;
    }

}
