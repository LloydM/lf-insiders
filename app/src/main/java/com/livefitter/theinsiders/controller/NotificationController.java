package com.livefitter.theinsiders.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.activity.NotificationDetailActivity;
import com.livefitter.theinsiders.adapter.NotificationAdapter;
import com.livefitter.theinsiders.listener.NotificationClickListener;
import com.livefitter.theinsiders.model.Notification;
import com.livefitter.theinsiders.request.RetrieveNotificationsRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by EJ Del Rosario
 * 27/06/2017
 * ejdelros08@gmail.com | ej.delrosario@fibo.ph
 * ------------------------
 * Copyright (c) Fibo
 * All Rights Reserved
 */

public class NotificationController extends BaseController implements NotificationClickListener {

    private SwipeRefreshLayout mRefreshLayout;
    private RecyclerView mRecyclerView;
    private NotificationAdapter mAdapter;

    public NotificationController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        ((MainActivity) getActivity()).setupDrawerWithToolbar(mToolbar);
        getActivity().getSupportActionBar().setHomeButtonEnabled(true);
        getActivity().getSupportActionBar().setTitle(getActivity().getString(R.string.title_notifications));

        mRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.refresh_layout);
        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);

        mRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                retrieveNotifications();

            }
        });

        retrieveNotifications();

    }

    private void retrieveNotifications(){

        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {

                mRefreshLayout.setRefreshing(true);

            }
        });

        RetrieveNotificationsRequest request = new RetrieveNotificationsRequest(getActivity(), new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        mRefreshLayout.post(new Runnable() {
                            @Override
                            public void run() {

                                mRefreshLayout.setRefreshing(false);

                            }
                        });

                        if(getFragment().isAdded()) {

                            Snackbar.make(getFragment().getView(),
                                    getActivity().getString(R.string.error_generic_error),
                                    Snackbar.LENGTH_SHORT).
                                    setAction(getActivity().getString(R.string.label_retry), new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            retrieveNotifications();
                                        }
                                    }).
                                    show();
                        }

                    }
                });

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {

                if(response.isSuccessful()){

                    String errorMessage = null;

                    try {

                        JSONObject jobj = new JSONObject(response.body().string());

                        boolean statusOK = jobj.getBoolean("status");

                        if(statusOK){

                            Gson gson = new Gson();
                            Type type = new TypeToken<ArrayList<Notification>>(){}.getType();
                            final ArrayList<Notification> notifications = gson.fromJson(jobj.getJSONArray("data").toString(), type);

                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    handleResponse(notifications);

                                }
                            });

                        }
                        else{

                            errorMessage = jobj.getString("message");

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();

                        errorMessage = getActivity().getString(R.string.error_generic_error);

                    }

                    if(errorMessage != null){

                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                errorMessage);

                    }

                }

            }
        });

        request.execute();

    }

    private void handleResponse(ArrayList<Notification> notifications) {

        mRefreshLayout.setRefreshing(false);

        mAdapter = new NotificationAdapter(getActivity(), notifications, this);

        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onClick(Notification notification) {

        notification.setRead(true);

        Intent intent = new Intent(getActivity(), NotificationDetailActivity.class);
        intent.putExtra(AppConstants.KEY_NOTIFICATION_ID, notification.getId());

        getActivity().switchActivity(intent, false);

    }
}
