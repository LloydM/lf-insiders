package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.linkedin.platform.APIHelper;
import com.linkedin.platform.errors.LIApiError;
import com.linkedin.platform.listeners.ApiListener;
import com.linkedin.platform.listeners.ApiResponse;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.activity.WelcomeActivity;
import com.livefitter.theinsiders.fragment.SignUpFragment;
import com.livefitter.theinsiders.model.UserProfile;
import com.livefitter.theinsiders.request.LinkedInSignUpRequest;
import com.livefitter.theinsiders.request.VerifyActivationCodeRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FaqTermsUtil;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 2/8/17
 * for Livefitter
 */

public class ActivationController extends BaseController implements OnClickListener, Callback {

    private TextInputLayout tilActivationCode;
    private TextInputEditText etActivationCode;
    private Button btSignUp;
    private FrameLayout btLinkedIn;
    private ProgressDialog dProgress;
    private TextView tvFaqTerms;

    // Temporary cached value of activation code
    private String mActivationCode;

    /**
     * Flag that determines which button they used to submit the activation code.
     * Whether via LinkedIn or manual registration
     */
    private boolean isUsingManualRegistration;

    public ActivationController(BaseActivity activity, BaseFragment fragment) {

        super(activity, fragment);
    }

    public void initialize(View rootView, Bundle savedInstanceState){
        tilActivationCode = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_activation_code);
        etActivationCode = (TextInputEditText) rootView.findViewById(R.id.edittext_activation_code);
        etActivationCode.addTextChangedListener(new FormFieldTextWatcher(tilActivationCode));
        etActivationCode.setFilters(new InputFilter[]{new EmojiExcludeFilter()});

        btSignUp = (Button) rootView.findViewById(R.id.button_sign_up);
        btLinkedIn = (FrameLayout) rootView.findViewById(R.id.button_sign_up_linkedin);

        btSignUp.setOnClickListener(this);
        btLinkedIn.setOnClickListener(this);

        tvFaqTerms = (TextView) rootView.findViewById(R.id.textview_faq_terms);
        FaqTermsUtil.setSpannableContent(getActivity(), tvFaqTerms);
    }

    @Override
    public void onClick(View view) {
        // Remove any residual values from mActivationCode
        mActivationCode = "";

        if(view == btSignUp || view == btLinkedIn){
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(etActivationCode.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            if(etActivationCode.getText().length() > 0) {
                isUsingManualRegistration = view == btSignUp;
                verifyActivationCode(etActivationCode.getText().toString().trim());
            }else{
                String activationCodeField = getActivity().getString(R.string.label_activation_code);
                String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, activationCodeField);
                tilActivationCode.setError(requiredFieldErrorMessage);
            }
        }
    }

    private void verifyActivationCode(String activationCode) {
        VerifyActivationCodeRequest mRequest = new VerifyActivationCodeRequest(getActivity(), activationCode, this);

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getResources().getString(R.string.label_verifying_activation_code),
                getActivity().getResources().getString(R.string.label_loading),
                false);

        mRequest.execute();
    }

    @Override
    public void onFailure(Call call, IOException e) {

        Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }
        });
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {

        if(dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if(response.isSuccessful()) {
            String responseBody = response.body().string();
            if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                Log.d(mFragment.getClassTag(), "onResponse body: " + responseBody);

                try {
                    JSONObject responseBodyJson = new JSONObject(responseBody);

                    if(responseBodyJson.has("status") && responseBodyJson.has("message")) {
                        boolean statusOK = responseBodyJson.getBoolean("status");
                        String messageResponse = responseBodyJson.getString("message");

                        if(!statusOK) {
                            if (messageResponse != null && !messageResponse.isEmpty()) {
                                showThreadSafeAlertDialog("", messageResponse, new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        etActivationCode.requestFocus();
                                    }
                                });
                            }
                        }else{
                            continueToSignUp(etActivationCode.getText().toString());
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));
                }
            }
        }
    }

    private void continueToSignUp(final String activationCode) {
        Log.i(getFragment().getClassTag(), "Activation code validated - sign up mode is: " + (isUsingManualRegistration? "Manual" : "LinkedIn"));

        // Store the validated activation code for later use
        mActivationCode = activationCode;

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if(isUsingManualRegistration){
                    Bundle arguments = new Bundle();
                    arguments.putString(AppConstants.KEY_ACTIVATION_CODE, activationCode);

                    SignUpFragment signUpFragment = new SignUpFragment();
                    signUpFragment.setArguments(arguments);
                    ((WelcomeActivity) getActivity()).switchViewPagerFragment(mFragment, signUpFragment);
                }else{
                    //have an own checker for linkedIn app since built it checker brings issues on some devices
                    Intent linkedIn = getActivity().getPackageManager().getLaunchIntentForPackage("com.linkedin.android");
                    if(linkedIn != null) {
                        initLinkedInAuthentication();
                    }
                    else{
                        DialogUtil.showConfirmationDialog(getActivity(),
                                getActivity().getString(R.string.title_install_linkedin),
                                getActivity().getString(R.string.msg_install_linkedin),
                                getActivity().getString(R.string.label_download),
                                getActivity().getString(R.string.label_cancel), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        if(which == DialogInterface.BUTTON_POSITIVE){
                                            getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "com.linkedin.android")));
                                        }
                                    }
                                });
                    }
                }
            }
        });
    }

    private void initLinkedInAuthentication() {

        if(getActivity() != null && getActivity() instanceof WelcomeActivity) {
            ((WelcomeActivity) getActivity()).initializeLinkedInAuthentication();
        }
    }

    public void onLinkedInSessionReady(){

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getResources().getString(R.string.label_sign_up),
                getActivity().getResources().getString(R.string.label_creating_account),
                false);

        String url = AppConstants.linkedInUrlCompleteProfileFields();

        APIHelper apiHelper = APIHelper.getInstance(getActivity());
        apiHelper.getRequest(getActivity(), url, new ApiListener() {
            @Override
            public void onApiSuccess(ApiResponse apiResponse) {
                // Success!
                Log.d("~~", "onApiSuccess apiResponse: " + apiResponse.toString());
                Log.d("~~", "onApiSuccess apiResponse data: " + apiResponse.getResponseDataAsString());

                boolean responseHandled = false;

                JSONObject responseDataJson = apiResponse.getResponseDataAsJson();

                if(responseDataJson != null){

                    try {
                        responseDataJson.put("activation_code", mActivationCode);

                        String token = FirebaseInstanceId.getInstance().getToken();

                        if(token != null && !token.isEmpty()){
                            responseDataJson.put("notification_token", token);
                            responseDataJson.put("device_type_id", AppConstants.API_DEVICE_TYPE_ID);
                        }

                        if(responseDataJson.has("activation_code")){
                            responseHandled = true;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                if(responseHandled){
                    signUpUserViaLinkedIn(responseDataJson);
                }else{

                    if(dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    DialogUtil.showAlertDialog(getActivity(),
                            getActivity().getString(R.string.label_linkedin),
                            getActivity().getString(R.string.error_linkedin_auth));
                }
            }

            @Override
            public void onApiError(LIApiError liApiError) {
                Log.e(getFragment().getClassTag(), "LIAuthError: " + liApiError.toString());
                // Error making GET request!
                if(dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_linkedin),
                        getActivity().getString(R.string.error_linkedin_auth));
            }
        });
    }


    public void onLinkedInSessionError(String message) {
        if(dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if (message.isEmpty()) {
            message = getActivity().getString(R.string.error_linkedin_auth);
        }
        DialogUtil.showAlertDialog(getActivity(),
                getActivity().getString(R.string.label_linkedin),
                message);
    }

    private void signUpUserViaLinkedIn(JSONObject userDetailJsonParams){
        if(userDetailJsonParams != null){

        LinkedInSignUpRequest signUpRequest = new LinkedInSignUpRequest(getActivity(), userDetailJsonParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            if (dProgress != null && dProgress.isShowing()) {
                                dProgress.dismiss();
                            }

                            DialogUtil.showAlertDialog(getActivity(),
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error));

                        }
                    });
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if(response.isSuccessful()) {
                        String responseBody = response.body().string();
                        if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                            Log.d(mFragment.getClassTag(), "onResponse body: " + responseBody);

                            try {
                                JSONObject responseBodyJson = new JSONObject(responseBody);

                                if(responseBodyJson.has("status")
                                        && (responseBodyJson.has("data") || responseBodyJson.has("message"))) {
                                    boolean statusOK = responseBodyJson.getBoolean("status");

                                    if(statusOK) {

                                        String dataResponse = responseBodyJson.getString("data");

                                        Gson gson = new Gson();
                                        Type typeToken = new TypeToken<UserProfile>(){}.getType();
                                        UserProfile userProfile = gson.fromJson(dataResponse, typeToken);

                                        if(userProfile != null) {
                                            continueToSignUp(userProfile);
                                        }
                                    }else{
                                        String messageResponse = responseBodyJson.getString("message");
                                        if (messageResponse != null && !messageResponse.isEmpty()) {

                                            if(dProgress != null && dProgress.isShowing()) {
                                                dProgress.dismiss();
                                            }

                                            showThreadSafeAlertDialog(
                                                    getActivity().getString(R.string.label_error),
                                                    messageResponse);
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if(dProgress != null && dProgress.isShowing()) {
                                            dProgress.dismiss();
                                        }
                                    }
                                });
                                showThreadSafeAlertDialog(
                                        getActivity().getString(R.string.label_error),
                                        getActivity().getString(R.string.error_generic_error));
                            }
                        }
                    }
                }
            });

            signUpRequest.execute();
        }
    }

    private void continueToSignUp(final UserProfile userProfile) {

        Log.d("~~", "continueToSignUp user profile: " + userProfile);

        if(!userProfile.getAuthenticationToken().isEmpty()) {
            Log.d("~~", "continueToSignUp save authtoken");
            SharedPreferencesHelper.saveAuthToken(getActivity(), userProfile.getAuthenticationToken());
            // And clear the salutations flag as this is a new user
            SharedPreferencesHelper.clearHasChosenSalutations(getActivity());
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(AppConstants.KEY_USER_PROFILE, userProfile);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
