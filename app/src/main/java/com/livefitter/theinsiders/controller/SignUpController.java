package com.livefitter.theinsiders.controller;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.model.UserProfile;
import com.livefitter.theinsiders.request.SignUpRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FaqTermsUtil;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;
import com.livefitter.theinsiders.view.DatePickerDialogFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ejdelrosario.framework.utilities.DateUtil;
import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 2/9/17
 * for Livefitter
 */

public class SignUpController extends BaseController implements DatePickerDialog.OnDateSetListener, View.OnClickListener, Callback {


    private String mActivationCode;

    private TextInputLayout tilFirstName, tilLastName, tilEmail, tilPassword, tilMobileNumber, tilBirthday;
    private TextInputEditText etFirstName, etLastName, etEmail, etPassword, etMobileNumber, etBirthday;
    private Button btnSignUp;

    private ProgressDialog dProgress;

    private DatePickerDialogFragment dDatePicker;

    private TextView tvFaqTerms;

    public SignUpController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {
        tilFirstName = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_first_name);
        tilLastName = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_last_name);
        tilEmail = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_email);
        tilPassword = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_password);
        tilMobileNumber = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_mobile);
        tilBirthday = (TextInputLayout) rootView.findViewById(R.id.edittext_layout_birthday);

        InputFilter[] inputFilters = new InputFilter[]{new EmojiExcludeFilter()};

        etFirstName = (TextInputEditText) rootView.findViewById(R.id.edittext_first_name);
        etFirstName.addTextChangedListener(new FormFieldTextWatcher(tilFirstName));
        etFirstName.setFilters(inputFilters);

        etLastName = (TextInputEditText) rootView.findViewById(R.id.edittext_last_name);
        etLastName.addTextChangedListener(new FormFieldTextWatcher(tilLastName));
        etLastName.setFilters(inputFilters);

        etEmail = (TextInputEditText) rootView.findViewById(R.id.edittext_email);
        etEmail.addTextChangedListener(new FormFieldTextWatcher(tilEmail));
        etEmail.setFilters(inputFilters);

        etPassword = (TextInputEditText) rootView.findViewById(R.id.edittext_password);
        etPassword.addTextChangedListener(new FormFieldTextWatcher(tilPassword));
        etPassword.setFilters(inputFilters);

        etMobileNumber = (TextInputEditText) rootView.findViewById(R.id.edittext_mobile);
        etMobileNumber.addTextChangedListener(new FormFieldTextWatcher(tilMobileNumber));
        etMobileNumber.setFilters(inputFilters);

        etBirthday = (TextInputEditText) rootView.findViewById(R.id.edittext_birthday);
        etBirthday.addTextChangedListener(new FormFieldTextWatcher(tilBirthday));
        etBirthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    // Forcing the soft keyboard to close
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    showDateDialog();
                } else {
                    hideDateDialog();
                }
            }
        });
        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });

        btnSignUp = (Button) rootView.findViewById(R.id.button_sign_up);
        btnSignUp.setOnClickListener(this);

        tvFaqTerms = (TextView) rootView.findViewById(R.id.textview_faq_terms);
        FaqTermsUtil.setSpannableContent(getActivity(), tvFaqTerms);
    }

    private void showDateDialog() {
        dDatePicker = new DatePickerDialogFragment();
        dDatePicker.setDateSetListener(this);
        dDatePicker.show(getActivity().getSupportFragmentManager(), "");
    }

    private void hideDateDialog() {
        if (dDatePicker.isVisible()) {
            dDatePicker.dismiss();
        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        // Convert the date into something readable
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        SimpleDateFormat format = new SimpleDateFormat(AppConstants.DATE_BIRTHDATE_FORMAT);
        String convertedDate = format.format(calendar.getTime());

        etBirthday.setText(convertedDate);
    }

    @Override
    public void onClick(View view) {
        if(view == btnSignUp){
            // Close the soft keyboard
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

            if(areFieldsValid()){
                JSONObject jsonParams = null;
                try {
                    jsonParams = createJsonParams();

                    if(jsonParams != null && !mActivationCode.isEmpty()) {
                        signUpUser(jsonParams);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private boolean areFieldsValid() {
        boolean areFieldsValid = true;

        //Check for field completion
        if(etFirstName.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_first_name);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilFirstName.setError(requiredFieldErrorMessage);
        }

        if(etLastName.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_last_name);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilLastName.setError(requiredFieldErrorMessage);
        }

        if(etEmail.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_email);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilEmail.setError(requiredFieldErrorMessage);
        }else if(!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()){
            areFieldsValid = false;
            String invalidEmailErrorMessage = getActivity().getString(R.string.error_invalid_email);
            tilEmail.setError(invalidEmailErrorMessage);
        }

        if(etPassword.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_password);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilPassword.setError(requiredFieldErrorMessage);
        }

        if(etMobileNumber.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_mobile_number);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilMobileNumber.setError(requiredFieldErrorMessage);
        }

        if(etBirthday.getText().toString().trim().length() == 0){
            areFieldsValid = false;
            String fieldName = getActivity().getString(R.string.label_birthday);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilBirthday.setError(requiredFieldErrorMessage);
        }
        else{

            Calendar calYearMinimumLimit = Calendar.getInstance();
            calYearMinimumLimit.add(Calendar.YEAR, -18);

            Calendar calSelected = Calendar.getInstance();
            calSelected.setTime(DateUtil.parseDate(AppConstants.DATE_BIRTHDATE_FORMAT, etBirthday.getText().toString()));

            if(calSelected.get(Calendar.YEAR) > calYearMinimumLimit.get(Calendar.YEAR)){

                areFieldsValid = false;
                String errorMessage = getActivity().getString(R.string.error_age_limit);
                tilBirthday.setError(errorMessage);

            }

        }

        return areFieldsValid;
    }

    private JSONObject createJsonParams() throws JSONException {
        JSONObject jsonParams = new JSONObject();

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String dateOfBirth = etBirthday.getText().toString().trim();
        String mobileNo = etMobileNumber.getText().toString().trim();

        JSONObject jsonData = new JSONObject();
        jsonData.put("email", email);
        jsonData.put("password", password);
        jsonData.put("first_name", firstName);
        jsonData.put("last_name", lastName);
        jsonData.put("dob", dateOfBirth);
        jsonData.put("mobile_no", mobileNo);
        jsonData.put("activation_code", mActivationCode);

        String token = FirebaseInstanceId.getInstance().getToken();

        if(token != null && !token.isEmpty()){
            jsonData.put("notification_token", token);
            jsonData.put("device_type_id", AppConstants.API_DEVICE_TYPE_ID);
        }

        jsonParams.put("data", jsonData);

        return jsonParams;
    }

    private void signUpUser(JSONObject jsonParams) {
        Log.d("~~", "signUpUser jsonParams: " + jsonParams);

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                getActivity().getResources().getString(R.string.label_sign_up),
                getActivity().getResources().getString(R.string.label_creating_account),
                false);
        SignUpRequest signUpRequest = new SignUpRequest(getActivity(), jsonParams, this);
        signUpRequest.execute();
    }

    @Override
    public void onResponse(Call call, Response response) throws IOException {
        if(dProgress != null && dProgress.isShowing()) {
            dProgress.dismiss();
        }

        if(response.isSuccessful()) {
            String responseBody = response.body().string();
            if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                Log.d(mFragment.getClassTag(), "onResponse body: " + responseBody);

                try {
                    JSONObject responseBodyJson = new JSONObject(responseBody);

                    if(responseBodyJson.has("status") && responseBodyJson.has("message")) {
                        boolean statusOK = responseBodyJson.getBoolean("status");

                        if(statusOK) {

                            String dataResponse = responseBodyJson.getString("data");

                            Gson gson = new Gson();
                            Type typeToken = new TypeToken<UserProfile>(){}.getType();
                            UserProfile userProfile = gson.fromJson(dataResponse, typeToken);

                            if(userProfile != null) {
                                continueToSignUp(userProfile);
                            }
                        }else{
                            String messageResponse = responseBodyJson.getString("message");
                            if (messageResponse != null && !messageResponse.isEmpty()) {
                                showThreadSafeAlertDialog(
                                        getActivity().getString(R.string.label_error),
                                        messageResponse);
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    showThreadSafeAlertDialog(
                            getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));
                }
            }
        }
    }

    @Override
    public void onFailure(Call call, IOException e) {

        Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));

            }
        });
    }

    private void continueToSignUp(final UserProfile userProfile) {

        Log.d("~~", "continueToSignUp user profile: " + userProfile);

        if(!userProfile.getAuthenticationToken().isEmpty()) {
            Log.d("~~", "continueToSignUp save authtoken");
            SharedPreferencesHelper.saveAuthToken(getActivity(), userProfile.getAuthenticationToken());
            // And clear the salutations flag as this is a new user
            SharedPreferencesHelper.clearHasChosenSalutations(getActivity());
        }

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Intent intent = new Intent(getActivity(), MainActivity.class);
                intent.putExtra(AppConstants.KEY_USER_PROFILE, userProfile);
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });
    }

    public void setActivationCode(String activationCode) {
        this.mActivationCode = activationCode;
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
