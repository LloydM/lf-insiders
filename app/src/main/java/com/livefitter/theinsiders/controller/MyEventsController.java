package com.livefitter.theinsiders.controller;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.adapter.MyEventsPagerAdapter;
import com.livefitter.theinsiders.fragment.InterestEventsFragment;
import com.livefitter.theinsiders.fragment.PastEventsFragment;
import com.livefitter.theinsiders.fragment.UpcomingEventsFragment;

import java.util.ArrayList;

/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class MyEventsController extends BaseController {

    private MyEventsPagerAdapter mSectionsPagerAdapter;

    UpcomingEventsFragment fragUpcomingEvents;
    PastEventsFragment fragPastEvents;
    InterestEventsFragment fragInterestEvents;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    public MyEventsController(BaseActivity activity, BaseFragment fragment){
        super(activity, fragment);
    }

    public void initialize(View rootView, Bundle savedInstanceState){

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        MainActivity homeActivity = (MainActivity) getActivity();
        homeActivity.setSupportActionBar(mToolbar);
        homeActivity.getSupportActionBar().setTitle(R.string.title_my_events);
        homeActivity.setupDrawerWithToolbar(mToolbar);
        homeActivity.getSupportActionBar().setHomeButtonEnabled(true);

        fragUpcomingEvents = new UpcomingEventsFragment();
        fragPastEvents = new PastEventsFragment();
        fragInterestEvents = new InterestEventsFragment();
        ArrayList<BaseFragment> fragmentArrayList = new ArrayList<>();

        fragmentArrayList.add(fragUpcomingEvents);
        fragmentArrayList.add(fragPastEvents);
        fragmentArrayList.add(fragInterestEvents);


        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new MyEventsPagerAdapter(getActivity(), getFragment().getChildFragmentManager(), fragmentArrayList);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) rootView.findViewById(R.id.viewpager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = (TabLayout) rootView.findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(mViewPager);

    }

    @Override
    public void onStop() {

    }
}
