package com.livefitter.theinsiders.controller;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.databinding.FragmentRsvpBinding;
import com.livefitter.theinsiders.model.EventModel;
import com.livefitter.theinsiders.model.GuestInvitation;
import com.livefitter.theinsiders.request.RetrieveEventInvitationsRequest;
import com.livefitter.theinsiders.request.UpdateGuestRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.SharedPreferencesHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;

import ejdelrosario.framework.utilities.DialogUtil;
import ejdelrosario.framework.utilities.FileUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/6/17
 * for Livefitter
 */

public class RsvpController extends BaseController
    implements View.OnClickListener{

    private static final String TAG = RsvpController.class.getSimpleName();

    private FragmentRsvpBinding mBinding;

    private EventModel mEvent;

    private FrameLayout flBlocker, flInvitationContainer;
    private ImageView btnPrevious, btnNext;
    private TextView tvCurrentIndex, tvMaxIndex;
    private ProgressDialog dProgress;

    private int mCurrentInviteIndex = 0;
    private ArrayList<GuestInvitation> mInvitationList;
    private GuestInvitation mCurrentInvitation;
    private static AsyncTask<Void, Void, String> asyncTask;

    public RsvpController(BaseActivity activity, BaseFragment fragment) {
        super(activity, fragment);

        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null && extras.containsKey(AppConstants.KEY_EVENT_MODEL)){
            mEvent = extras.getParcelable(AppConstants.KEY_EVENT_MODEL);

            retrieveInvitationsForEvent();
        }
    }

    private void retrieveInvitationsForEvent() {
        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_retrieving_event_invitations),
                false);

        RetrieveEventInvitationsRequest request = new RetrieveEventInvitationsRequest(getActivity(), mEvent.getId(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status") && responseBodyJson.has("data")) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                if (statusOK) {

                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new Gson();
                                    Type typeToken = new TypeToken<ArrayList<GuestInvitation>>() {}.getType();
                                    final ArrayList<GuestInvitation> guestInvitationList = gson.fromJson(dataResponse, typeToken);

                                    if (guestInvitationList != null) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                handleResponse(guestInvitationList);
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error),
                                    new DialogInterface.OnDismissListener() {
                                        @Override
                                        public void onDismiss(DialogInterface dialogInterface) {
                                            getActivity().finish();
                                        }
                                    });
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error),
                                new DialogInterface.OnDismissListener() {
                                    @Override
                                    public void onDismiss(DialogInterface dialogInterface) {
                                        getActivity().finish();
                                    }
                                });

                    }
                });
            }
        });

        request.execute();
    }

    private void handleResponse(final ArrayList<GuestInvitation> guestInvitationList){
        flBlocker.setVisibility(View.GONE);
        mInvitationList = guestInvitationList;
        // Clone a new instance of the current guest invite
        // as we're going to change its fields in the setGuestInvite()
        mCurrentInvitation = mInvitationList.get(mCurrentInviteIndex).newInstance();
        mBinding.setGuestInvite(mCurrentInvitation);
        mBinding.setInviteImageUrl(mEvent.getInviteImageUrl());

        if(mInvitationList.size() == 1){
            btnNext.setVisibility(View.INVISIBLE);
        }

        tvCurrentIndex.setText(String.valueOf(mCurrentInviteIndex + 1));
        tvMaxIndex.setText(String.valueOf(mInvitationList.size()));

        // Determine if we should show the instruction dialog or not
        boolean knowsInviteInstructions = SharedPreferencesHelper.knowsInviteInstructions(getActivity());

        if(!knowsInviteInstructions){
            showInstructionsDialog(true);
        }
    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        ((BaseActivity)getActivity()).setSupportActionBar(mToolbar);
        ((BaseActivity)getActivity()).enableUpbutton();

        flBlocker = (FrameLayout) rootView.findViewById(R.id.loading_blocker);
        flInvitationContainer = (FrameLayout) rootView.findViewById(R.id.fl_invitation_container);
        btnPrevious = (ImageView) rootView.findViewById(R.id.rsvp_previous_button);
        btnNext = (ImageView) rootView.findViewById(R.id.rsvp_next_button);
        tvCurrentIndex = (TextView) rootView.findViewById(R.id.rsvp_guest_count);
        tvMaxIndex = (TextView) rootView.findViewById(R.id.rsvp_guest_max);

        flBlocker.setVisibility(View.VISIBLE);
        btnPrevious.setVisibility(View.INVISIBLE);

        btnPrevious.setOnClickListener(this);
        btnNext.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if(view == btnPrevious && mCurrentInviteIndex > 0){
            mCurrentInviteIndex--;
            GuestInvitation guestInvite = mInvitationList.get(mCurrentInviteIndex);
            setGuestInvite(guestInvite);
        }else if(view == btnNext && mCurrentInviteIndex < mInvitationList.size() - 1){
            mCurrentInviteIndex++;
            GuestInvitation guestInvite = mInvitationList.get(mCurrentInviteIndex);
            setGuestInvite(guestInvite);
        }
    }

    /**
     * Use the databound observable object of {@link GuestInvitation} to change the currently shown information on screen
     * Using a dummy guestInvitation object in mCurrentInvitation, change the relevant fields to change the info on screen.
     * Currently only changing the following:
     * <ul>
     *     <li>{@link GuestInvitation#salutation Salutation}</li>
     *     <li>{@link GuestInvitation#name Name}</li>
     *     <li>{@link GuestInvitation#qrCodeUrl QR Code URL}</li>
     * </ul>
     * @param guestInvitation basis of fields to be changed
     */
    private void setGuestInvite(GuestInvitation guestInvitation) {

        if(mCurrentInviteIndex == 0){
            btnPrevious.setVisibility(View.INVISIBLE);
        }else {
            btnPrevious.setVisibility(View.VISIBLE);
        }

        if(mCurrentInviteIndex == mInvitationList.size() - 1){
            btnNext.setVisibility(View.INVISIBLE);
        }else{
            btnNext.setVisibility(View.VISIBLE);
        }

        tvCurrentIndex.setText(String.valueOf(mCurrentInviteIndex + 1));

        mCurrentInvitation.setSalutation(guestInvitation.getSalutation());
        mCurrentInvitation.setName(guestInvitation.getName());
        mCurrentInvitation.setQrCodeUrl(guestInvitation.getQrCodeUrl());
    }

    public void showInstructionsDialog(final boolean changePreference) {
        DialogUtil.showAlertDialog(getActivity(),
                getFragment().getString(R.string.msg_rsvp_instructions),
                new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        if(changePreference) {
                            SharedPreferencesHelper.setKnowsInviteInstructions(getActivity());
                        }
                    }
                });
    }

    public void shareInvitation(){

        if(writePermissionGranted()){

            //run in async task to make process thread safety
            asyncTask = new AsyncTask<Void, Void, String>() {

                private ProgressDialog pdLoading;
                private Bitmap invitationBitmap;
                private String dir;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    pdLoading = new ProgressDialog(getActivity());
                    pdLoading.setCancelable(false);
                    pdLoading.setMessage(getActivity().getString(R.string.label_loading));
                    pdLoading.show();

                    //put these methods here because it requires to get called from UI thread
                    flInvitationContainer.buildDrawingCache(true);

                    invitationBitmap = Bitmap.createBitmap(flInvitationContainer.getWidth(), flInvitationContainer.getHeight(), Bitmap.Config.ARGB_8888);
                    Canvas canvas = new Canvas(invitationBitmap);
                    flInvitationContainer.draw(canvas);

                }

                @Override
                protected String doInBackground(Void... params) {

                    String errorMessage = null;

                    dir = Environment.getExternalStorageDirectory() + "/Android/data/" + getActivity().getPackageName() + "/";
                    Log.i(TAG, "directory: " + dir);

                    try {
                        FileUtil.saveImage(dir, "invitationImage.jpg", invitationBitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                        errorMessage = getActivity().getString(R.string.error_share_invitation);
                    }

                    return errorMessage;
                }

                @Override
                protected void onPostExecute(String errorMessage) {
                    super.onPostExecute(errorMessage);

                    pdLoading.dismiss();

                    if (errorMessage != null) {
                        DialogUtil.showAlertDialog(getActivity(), errorMessage);
                    } else {

                        Intent share = new Intent(Intent.ACTION_SEND);
                        /*share.setType("image/jpeg");
                        share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(dir + "invitationImage.jpg")));*/

                        File file = new File(dir + "invitationImage.jpg");
                        Uri imageUri = FileProvider.getUriForFile(
                                getFragment().getActivity(),
                                getFragment().getActivity().getApplicationContext()
                                        .getPackageName() + ".provider", file);
                        share.setDataAndType(imageUri, "image/jpeg");
                        share.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        getActivity().startActivity(Intent.createChooser(share, getActivity().getString(R.string.label_share_invitation)));

                    }
                }
            };

            asyncTask.execute();
        }
    }

    /**
     * checks write permission and initiates request if not permitted
     * @return whether write permission is granted
     */
    private boolean writePermissionGranted(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if(getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED){

                return true;

            }
            else {

                //it's important that you call requestPermissions method from fragment
                //to have the onRequestPermissionsResult invoked on Fragment instead of Activity
                getFragment().requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, AppConstants.PERM_SHARE_INVITATION);

                return false;
            }
        }
        else{

            return true;

        }
    }

    public void showEditNameDialog(){

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update_guest);

        final Spinner sSalutations = (Spinner) dialog.findViewById(R.id.event_detail_invite_salutation);
        final EditText etGuestName = (EditText) dialog.findViewById(R.id.event_detail_invite_name);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.array_salutations, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sSalutations.setAdapter(adapter);

        etGuestName.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if(isGuestNameValid(etGuestName.getText().toString())){

                        dialog.dismiss();
                        updateGuest(mCurrentInvitation.getName(), etGuestName.getText().toString().trim(), sSalutations.getSelectedItem().toString());

                    }

                    return true;
                }
                return false;
            }
        });

        //populate current user data
        int currentSalutationIndex = 0;

        switch (mCurrentInvitation.getSalutation()){

            case "Mr":{
                currentSalutationIndex = 0;
                break;
            }
            case "Ms":{
                currentSalutationIndex = 1;
                break;
            }
            case "Mrs":{
                currentSalutationIndex = 2;
                break;
            }
        }

        sSalutations.setSelection(currentSalutationIndex);

        etGuestName.setText(mCurrentInvitation.getName());
        etGuestName.setSelection(etGuestName.length());

        dialog.findViewById(R.id.btn_update_name).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(isGuestNameValid(etGuestName.getText().toString())){

                    dialog.dismiss();
                    updateGuest(mCurrentInvitation.getName(), etGuestName.getText().toString().trim(), sSalutations.getSelectedItem().toString());

                }

            }
        });

        dialog.show();
    }

    private boolean isGuestNameValid(String newName){

        if(newName.trim().length() > 0){

            return true;

        }
        else{
            DialogUtil.showAlertDialog(getActivity(),
                    getFragment().getString(R.string.label_error),
                    getFragment().getString(R.string.error_no_guests_selected));

            return false;
        }
    }

    private void updateGuest(String oldName, final String newName, final String newSalutation){

        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getActivity().getString(R.string.label_updating_guest_name),
                false);

        try {

            JSONObject jobjParams = createJsonParams(oldName, newName, newSalutation);

            UpdateGuestRequest mRequest = new UpdateGuestRequest(getActivity(), jobjParams, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    DialogUtil.showAlertDialog(getActivity(),
                            getActivity().getString(R.string.label_error),
                            getActivity().getString(R.string.error_generic_error));

                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {

                    if (dProgress != null && dProgress.isShowing()) {
                        dProgress.dismiss();
                    }

                    String errorMessage = null;

                    if(response.isSuccessful()){
                        String responseString = response.body().string();

                        try {

                            JSONObject jobj = new JSONObject(responseString);

                            boolean status = jobj.getBoolean("status");

                            if(!status){
                                errorMessage = jobj.getString("message");
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                            errorMessage = getActivity().getString(R.string.error_generic_error);
                        }
                    }
                    else{

                        errorMessage = getActivity().getString(R.string.error_generic_error);

                    }

                    if(errorMessage != null){
                        showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                errorMessage);
                    }
                    else{
                        //update cached values
                        final GuestInvitation guestInvite = mInvitationList.get(mCurrentInviteIndex);
                        guestInvite.setName(newName);
                        guestInvite.setSalutation(newSalutation);

                        //make changes on UI thread
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                setGuestInvite(guestInvite);
                            }
                        });
                    }
                }
            });

            mRequest.execute();

        } catch (JSONException e) {
            e.printStackTrace();

            if (dProgress != null && dProgress.isShowing()) {
                dProgress.dismiss();
            }

            DialogUtil.showAlertDialog(getActivity(),
                    getActivity().getString(R.string.label_error),
                    getActivity().getString(R.string.error_generic_error));
        }


    }

    private JSONObject createJsonParams(String oldName, String newName, String newSalutation) throws JSONException {

        JSONObject jobjParams = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("event_id", mEvent.getId());

        JSONArray jarrayGuests = new JSONArray();
        JSONObject jobjGuest = new JSONObject();

        jobjGuest.put("old_name", oldName);
        jobjGuest.put("new_name", newName);
        jobjGuest.put("new_salutation", newSalutation);

        jarrayGuests.put(jobjGuest);

        jobjData.put("guests", jarrayGuests);

        jobjParams.put("data", jobjData);

        return jobjParams;
    }

    public void setBinding(FragmentRsvpBinding binding) {
        this.mBinding = binding;
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }
}
