package com.livefitter.theinsiders.controller;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.ChangePasswordActivity;
import com.livefitter.theinsiders.model.UserProfile;
import com.livefitter.theinsiders.request.UpdateProfileRequest;
import com.livefitter.theinsiders.utility.DialogUtility;
import com.livefitter.theinsiders.utility.EmojiExcludeFilter;
import com.livefitter.theinsiders.utility.FormFieldTextWatcher;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Calendar;
import java.util.Date;

import ejdelrosario.framework.utilities.DateUtil;
import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LFT-PC-010 on 6/16/2017.
 */

public class UpdateProfileController extends BaseController {


    private UserProfile mUserProfile;

    private TextInputLayout tilFirstName, tilLastName, tilContact, tilBirthday;
    private TextInputEditText etFirstName, etLastName, etContact, etBirthday, etSalutation;
    private Button btnChangePassword;

    //for birth date picker; to retain selected date as initial date upon showing picker dialog
    private Calendar mCalendar;

    private PopupMenu mSalutationMenu;


    public UpdateProfileController(BaseActivity activity, BaseFragment fragment){
        super(activity, fragment);

        Bundle extras = getActivity().getIntent().getExtras();
        if(extras != null && extras.containsKey(AppConstants.KEY_USER_PROFILE)){
            mUserProfile = extras.getParcelable(AppConstants.KEY_USER_PROFILE);
        }

    }

    @Override
    public void initialize(View rootView, Bundle savedInstanceState) {

        mToolbar = rootView.findViewById(R.id.toolbar);
        getActivity().setSupportActionBar(mToolbar);
        getActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tilFirstName= rootView.findViewById(R.id.update_profile_edittext_first_name_layout);
        tilLastName = rootView.findViewById(R.id.update_profile_edittext_last_name_layout);
        tilContact  = rootView.findViewById(R.id.update_profile_edittext_contact_layout);
        tilBirthday = rootView.findViewById(R.id.update_profile_edittext_birthday_layout);

        etFirstName = rootView.findViewById(R.id.update_profile_edittext_first_name);
        etLastName  = rootView.findViewById(R.id.update_profile_edittext_last_name);
        etContact   = rootView.findViewById(R.id.update_profile_edittext_contact);
        etBirthday  = rootView.findViewById(R.id.update_profile_edittext_birthday);
        etSalutation= rootView.findViewById(R.id.update_profile_edittext_salutation);

        etFirstName.addTextChangedListener(new FormFieldTextWatcher(tilFirstName));
        etLastName.addTextChangedListener(new FormFieldTextWatcher(tilLastName));
        etContact.addTextChangedListener(new FormFieldTextWatcher(tilContact));
        etBirthday.addTextChangedListener(new FormFieldTextWatcher(tilBirthday));

        InputFilter[] inputFilters = new InputFilter[]{new EmojiExcludeFilter()};
        etFirstName.setFilters(inputFilters);
        etLastName.setFilters(inputFilters);
        etContact.setFilters(inputFilters);
        etBirthday.setFilters(inputFilters);

        etBirthday.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    // Forcing the soft keyboard to close
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);

                    showDatePickerDialog();
                }
            }
        });

        etBirthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        etSalutation.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(hasFocus){
                    showSalutationPopupMenu();
                }
            }
        });

        etSalutation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                showSalutationPopupMenu();

            }
        });

        //save changes button
        rootView.findViewById(R.id.update_profile_button_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                saveChanges();

            }
        });

        btnChangePassword = rootView.findViewById(R.id.update_profile_button_change_password);
        btnChangePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getActivity().switchActivity(ChangePasswordActivity.class, false);

            }
        });

        populateInitialData();

    }

    private void populateInitialData(){

        etFirstName.setText(mUserProfile.getFirstName());
        etLastName.setText(mUserProfile.getLastName());
        etContact.setText(mUserProfile.getMobileNumber());
        etBirthday.setText(mUserProfile.getDateOfBirth());
        etSalutation.setText(mUserProfile.getSalutation());

        mCalendar = Calendar.getInstance();
        if(mUserProfile.getDateOfBirth() != null && mUserProfile.getDateOfBirth().length() > 0) {
            Date birthDate = DateUtil.parseDate(AppConstants.DATE_BIRTHDATE_FORMAT, mUserProfile.getDateOfBirth());
            if (birthDate != null) {
                mCalendar.setTime(birthDate);
            }
        }

        int visibility = mUserProfile.isLinkedIn() ? View.GONE : View.VISIBLE;
        btnChangePassword.setVisibility(visibility);
    }

    private void showDatePickerDialog(){

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

                        mCalendar.set(Calendar.YEAR, year);
                        mCalendar.set(Calendar.MONTH, month);
                        mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                        etBirthday.setText(DateUtil.getDate(AppConstants.DATE_BIRTHDATE_FORMAT, mCalendar.getTime()));

                    }
                },
                mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH),
                mCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        datePickerDialog.show();

    }

    private void showSalutationPopupMenu(){

        if(mSalutationMenu == null){

            mSalutationMenu = new PopupMenu(getActivity(), etSalutation);
            mSalutationMenu.getMenu().add(getActivity().getString(R.string.salutations_mister));
            mSalutationMenu.getMenu().add(getActivity().getString(R.string.salutations_miss));
            mSalutationMenu.getMenu().add(getActivity().getString(R.string.salutations_misis));

            mSalutationMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {

                    etSalutation.setText(item.getTitle());

                    return true;
                }
            });
        }

        mSalutationMenu.show();
    }

    private boolean isValid(){

        boolean isValid = true;

        if(etFirstName.getText().toString().trim().length() <= 0){

            String fieldName = getActivity().getString(R.string.label_first_name);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilFirstName.setError(requiredFieldErrorMessage);

            isValid = false;
        }

        if(etLastName.getText().toString().trim().length() <= 0){

            String fieldName = getActivity().getString(R.string.label_last_name);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilLastName.setError(requiredFieldErrorMessage);

            isValid = false;
        }

        if(etContact.getText().toString().trim().length() <= 0){

            String fieldName = getActivity().getString(R.string.label_contact);
            String requiredFieldErrorMessage = getActivity().getString(R.string.error_required_field, fieldName);
            tilContact.setError(requiredFieldErrorMessage);

            isValid = false;
        }

        Calendar calYearMinimumLimit = Calendar.getInstance();
        calYearMinimumLimit.add(Calendar.YEAR, -18);

        if(mCalendar.get(Calendar.YEAR) > calYearMinimumLimit.get(Calendar.YEAR)){

            isValid = false;
            String errorMessage = getActivity().getString(R.string.error_age_limit);
            tilBirthday.setError(errorMessage);

        }

        return isValid;
    }

    private void saveChanges() {

        if(isValid()){

            final ProgressDialog pdLoading = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                    getActivity().getString(R.string.label_edit_info),
                    getActivity().getString(R.string.label_updating_info),
                    false);

            try {

                JSONObject jsonParams = createJsonParams();

                UpdateProfileRequest mRequest = new UpdateProfileRequest(getActivity(), jsonParams, mUserProfile.getId(), new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {

                        if(pdLoading != null && pdLoading.isShowing()){
                            pdLoading.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error));

                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {

                        if (pdLoading != null && pdLoading.isShowing()) {
                            pdLoading.dismiss();
                        }

                        String errorMessage = null;

                        if(response.isSuccessful()){

                            String responseString = response.body().string();

                            try {

                                JSONObject jobj = new JSONObject(responseString);

                                boolean status = jobj.getBoolean("status");

                                if(status){

                                    Gson gson = new Gson();
                                    Type type = new TypeToken<UserProfile>(){}.getType();

                                    mUserProfile = gson.fromJson(jobj.getJSONObject("data").toString(), type);

                                }
                                else{

                                    errorMessage = jobj.getString("message");

                                }

                            } catch (JSONException e) {
                                e.printStackTrace();

                                errorMessage = getActivity().getString(R.string.error_generic_error);

                            }
                        }
                        else{

                            errorMessage = getActivity().getString(R.string.error_generic_error);

                        }

                        if(errorMessage != null){

                            showThreadSafeAlertDialog(getActivity().getString(R.string.label_error),
                                    errorMessage);

                        }
                        else{

                            //send back updated user profile to previous fragment
                            getActivity().runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    Intent intent = new Intent();
                                    intent.putExtra(AppConstants.KEY_USER_PROFILE, mUserProfile);
                                    getActivity().setResult(Activity.RESULT_OK,  intent);
                                    getActivity().finish();

                                }
                            });
                        }

                    }
                });

                mRequest.execute();

            } catch (JSONException e) {
                e.printStackTrace();

                if(pdLoading != null && pdLoading.isShowing()){
                    pdLoading.dismiss();
                }

                DialogUtil.showAlertDialog(getActivity(),
                        getActivity().getString(R.string.label_error),
                        getActivity().getString(R.string.error_generic_error));
            }
        }

    }

    private JSONObject createJsonParams() throws JSONException {

        JSONObject jobjParams = new JSONObject();
        JSONObject jobjData = new JSONObject();

        jobjData.put("first_name", etFirstName.getText().toString());
        jobjData.put("last_name", etLastName.getText().toString());
        jobjData.put("dob", etBirthday.getText().toString());
        jobjData.put("mobile_no", etContact.getText().toString());
        jobjData.put("salutation", etSalutation.getText().toString());

        jobjParams.put("data", jobjData);

        return jobjParams;

    }

    @Override
    public void onStop() {

    }
}
