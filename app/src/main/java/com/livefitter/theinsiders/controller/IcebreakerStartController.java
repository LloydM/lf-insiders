package com.livefitter.theinsiders.controller;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.IcebreakerActivity;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.databinding.FragmentIcebreakerStartBinding;
import com.livefitter.theinsiders.model.Icebreaker;
import com.livefitter.theinsiders.request.RetrieveIcebreakerRequest;
import com.livefitter.theinsiders.utility.DialogUtility;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.Type;

import ejdelrosario.framework.utilities.DialogUtil;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class IcebreakerStartController extends BaseController implements View.OnClickListener {

    private FrameLayout flBlocker;
    private Button btnStart;
    private ProgressDialog dProgress;

    private FragmentIcebreakerStartBinding mBinding;
    private Icebreaker mIcebreaker;

    public IcebreakerStartController(BaseActivity activity, BaseFragment fragment){
        super(activity, fragment);

        retrieveIcebreaker();
    }

    public void initialize(View rootView, Bundle savedInstanceState){

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        MainActivity homeActivity = (MainActivity) getActivity();
        homeActivity.setSupportActionBar(mToolbar);
        homeActivity.getSupportActionBar().setTitle(R.string.title_icebreaker);
        homeActivity.setupDrawerWithToolbar(mToolbar);
        homeActivity.getSupportActionBar().setHomeButtonEnabled(true);

        flBlocker = (FrameLayout) rootView.findViewById(R.id.loading_blocker);
        flBlocker.setVisibility(View.VISIBLE);

        btnStart = (Button) rootView.findViewById(R.id.icebreaker_button_start);
        btnStart.setOnClickListener(this);
    }

    private void retrieveIcebreaker() {
        dProgress = DialogUtility.showIndeterminateProgressDialog(getActivity(),
                "",
                getFragment().getString(R.string.label_retrieving_icebreaker),
                false);

        RetrieveIcebreakerRequest request = new RetrieveIcebreakerRequest(getActivity(), new Callback() {

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (dProgress != null && dProgress.isShowing()) {
                    dProgress.dismiss();
                }

                if (response.isSuccessful()) {
                    String responseBody = response.body().string();
                    if (response.isSuccessful() && !responseBody.isEmpty() && !responseBody.equals("{}")) {
                        Log.d(getFragment().getClassTag(), "onResponse body: " + responseBody);

                        try {
                            JSONObject responseBodyJson = new JSONObject(responseBody);

                            if (responseBodyJson.has("status")
                                    && (responseBodyJson.has("data")
                                    || responseBodyJson.has("message") )) {
                                boolean statusOK = responseBodyJson.getBoolean("status");

                                // Test data/behavior
                                /*final Icebreaker icebreaker = new Icebreaker("Find your partner",
                                        "Find members with the same image as yours",
                                        "http://pngimg.com/uploads/bottle/bottle_PNG2058.png",
                                        true);

                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {

                                        flBlocker.setVisibility(View.GONE);
                                        mIcebreaker = icebreaker;
                                        mBinding.setIsEnabled(true);
                                        mBinding.setMessageString(mIcebreaker.getDescription());
                                    }
                                });*/

                                if (statusOK) {

                                    String dataResponse = responseBodyJson.getString("data");

                                    Gson gson = new Gson();
                                    Type typeToken = new TypeToken<Icebreaker>() {}.getType();
                                    final Icebreaker icebreaker = gson.fromJson(dataResponse, typeToken);

                                    if (icebreaker != null && icebreaker.getDescription() != null && !icebreaker.getDescription().isEmpty()) {
                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                flBlocker.setVisibility(View.GONE);
                                                mIcebreaker = icebreaker;
                                                mBinding.setIsEnabled(true);
                                                mBinding.setMessageString(mIcebreaker.getDescription());
                                            }
                                        });
                                    }
                                }else{

                                    final String messageResponse = responseBodyJson.getString("message");

                                    if(mBinding != null && !messageResponse.isEmpty()){

                                        getActivity().runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                flBlocker.setVisibility(View.GONE);
                                                mBinding.setIsEnabled(false);
                                                mBinding.setMessageString(messageResponse);
                                            }
                                        });
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            showThreadSafeAlertDialog(
                                    getActivity().getString(R.string.label_error),
                                    getActivity().getString(R.string.error_generic_error));
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call call, IOException e) {
                Log.d(getFragment().getClassTag(), "onFailure exception " + e.getMessage());

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (dProgress != null && dProgress.isShowing()) {
                            dProgress.dismiss();
                        }

                        DialogUtil.showAlertDialog(getActivity(),
                                getActivity().getString(R.string.label_error),
                                getActivity().getString(R.string.error_generic_error));

                    }
                });
            }
        });

        request.execute();
    }

    @Override
    public void onClick(View view) {
        if(view == btnStart && mIcebreaker != null){
            Intent intent = new Intent(getActivity(), IcebreakerActivity.class);
            intent.putExtra(AppConstants.KEY_ICEBREAKER, mIcebreaker);
            getFragment().startActivityForResult(intent, AppConstants.REQUEST_CODE_ICEBREAKER);
        }
    }

    public void setBinding(FragmentIcebreakerStartBinding binding) {
        this.mBinding = binding;
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == AppConstants.REQUEST_CODE_ICEBREAKER && resultCode == AppCompatActivity.RESULT_OK){
            // We set the isRedeemed value to true as we have confirmed in IcebreakerActivity that it was successfully redeemed.
            // We just reflect this change here to avoid the awkward case where they just finished redeeming
            // and going back here, then back to the Icebreaker makes it seem like it wasnt redeemed
            mIcebreaker.setRedeemed(true);
        }
    }
}
