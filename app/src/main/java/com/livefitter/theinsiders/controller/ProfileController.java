package com.livefitter.theinsiders.controller;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.livefitter.theinsiders.AppConstants;
import com.livefitter.theinsiders.BaseActivity;
import com.livefitter.theinsiders.BaseFragment;
import com.livefitter.theinsiders.BuildConfig;
import com.livefitter.theinsiders.R;
import com.livefitter.theinsiders.activity.AddCreditCardActivity;
import com.livefitter.theinsiders.activity.CreditCardListActivity;
import com.livefitter.theinsiders.activity.MainActivity;
import com.livefitter.theinsiders.databinding.FragmentProfileBinding;
import com.livefitter.theinsiders.model.UserProfile;

/**
 * Created by LloydM on 3/7/17
 * for Livefitter
 */

public class ProfileController extends BaseController implements View.OnClickListener {

    private Button btnChangePaymentDetails;
    private ProgressDialog dProgress;

    private FragmentProfileBinding mBinding;
    private UserProfile mUserProfile;

    public ProfileController(BaseActivity activity, BaseFragment fragment){
        super(activity, fragment);

        Bundle extras = getFragment().getArguments();
        if(extras != null && extras.containsKey(AppConstants.KEY_USER_PROFILE)){
            mUserProfile = extras.getParcelable(AppConstants.KEY_USER_PROFILE);
        }
    }

    public void initialize(View rootView, Bundle savedInstanceState){

        mToolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        MainActivity homeActivity = (MainActivity) getActivity();
        homeActivity.setSupportActionBar(mToolbar);
        homeActivity.getSupportActionBar().setTitle(R.string.title_profile);
        homeActivity.setupDrawerWithToolbar(mToolbar);
        homeActivity.getSupportActionBar().setHomeButtonEnabled(true);

        btnChangePaymentDetails = (Button) rootView.findViewById(R.id.profile_button_change_payment_details);
        btnChangePaymentDetails.setOnClickListener(this);

        if(mBinding != null && mUserProfile != null){
            mBinding.setProfile(mUserProfile);

            String versionName = BuildConfig.VERSION_NAME;

            if(!versionName.isEmpty()){
                String versionString = getFragment().getString(R.string.label_version);
                versionString = versionString.concat(" " + versionName);
                mBinding.setVersionName(versionString);
            }

            mBinding.executePendingBindings();
        }
    }

    @Override
    public void onClick(View view) {
        if(view == btnChangePaymentDetails && mUserProfile != null){
            getActivity().switchActivity(CreditCardListActivity.class, false);
        }
    }

    public void setBinding(FragmentProfileBinding binding) {
        this.mBinding = binding;
    }

    @Override
    public void onStop() {
        if(dProgress != null && dProgress.isShowing()){
            dProgress.dismiss();
            dProgress = null;
        }
    }

    public void updateUserValues(UserProfile newUserProfile) {

        mUserProfile = newUserProfile;
        mBinding.setProfile(mUserProfile);
        mBinding.executePendingBindings();

    }
}
