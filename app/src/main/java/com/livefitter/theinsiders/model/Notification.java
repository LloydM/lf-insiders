package com.livefitter.theinsiders.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.livefitter.theinsiders.BR;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Created by LFT-PC-010 on 6/27/2017.
 */

public class Notification extends BaseObservable implements Parcelable{

    private int id;
    @SerializedName("date_time")
    private String dateTime;
    private String title;
    private String message;
    @SerializedName("is_read")
    private boolean isRead;

    protected Notification(Parcel in) {
        id = in.readInt();
        dateTime = in.readString();
        title = in.readString();
        message = in.readString();
        isRead = in.readByte() != 0;
    }

    public static final Creator<Notification> CREATOR = new Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel in) {
            return new Notification(in);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(dateTime);
        dest.writeString(title);
        dest.writeString(message);
        dest.writeByte((byte) (isRead ? 1 : 0));
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Bindable
    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
        notifyPropertyChanged(BR.read);
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
