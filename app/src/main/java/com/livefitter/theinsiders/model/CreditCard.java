package com.livefitter.theinsiders.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import com.google.gson.annotations.SerializedName;
import com.livefitter.theinsiders.BR;

/**
 * Created by LFT-PC-010 on 7/16/2017.
 */

public class CreditCard extends BaseObservable {


    /**
     * brand : Visa
     * created_at : 2017-07-16T18:53:07+08:00
     * exp_month : 11
     * exp_year : 2018
     * id : 2
     * is_default : true
     * last4 : 4242
     * stripe_card_id : card_1AgA7qHdRx9A2vrGbsHxB30i
     * updated_at : 2017-07-16T18:53:07+08:00
     * user_id : 145
     */

    private String brand;
    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("exp_month")
    private int expMonth;
    @SerializedName("exp_year")
    private int expYear;
    private int id;
    @SerializedName("is_default")
    private boolean isDefault;
    private int last4;
    @SerializedName("stripe_card_id")
    private String stripeCardId;
    @SerializedName("updated_at")
    private String updatedAt;
    @SerializedName("user_id")
    private int userId;
    private boolean isSelected;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public int getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(int expMonth) {
        this.expMonth = expMonth;
    }

    public int getExpYear() {
        return expYear;
    }

    public void setExpYear(int expYear) {
        this.expYear = expYear;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isIsDefault() {
        return isDefault;
    }

    public void setIsDefault(boolean isDefault) {
        this.isDefault = isDefault;
    }

    public int getLast4() {
        return last4;
    }

    public void setLast4(int last4) {
        this.last4 = last4;
    }

    public String getStripeCardId() {
        return stripeCardId;
    }

    public void setStripeCardId(String stripeCardId) {
        this.stripeCardId = stripeCardId;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Bindable
    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
        notifyPropertyChanged(BR.selected);
    }
}
