package com.livefitter.theinsiders.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.livefitter.theinsiders.BR;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 1/30/17
 * for Livefitter
 */

public class EventModel extends BaseObservable implements Parcelable {
    /*"address": "Urban Avenue, Makati",
    "date_time": "17 January 2017, 07:00 PM",
    "end_date": "2017-02-15",
    "end_time": "2000-01-01T22:00:00Z",
    "event_image_url": null,
    "event_description": "This is the best event EVER!",
    "has_rsvped": true,
    "id": 5,
    "invite_description": "Come and join us!",
    "max_add_guests": 3,
    "remaining_additional_guests": 3,
    "remaining_slots": 10,
    "sponsor_logo_url": null,
    "start_date": "2017-02-15",
    "start_time": "2000-01-01T19:00:00Z",
    "title": "The Best EventModel",
    "is_crowd_sourced": false,
    "has_expressed_interest": false,
    "event_type_id": 1,
    "price": "0.0"*/

    private String address;
    @SerializedName("date_time")
    private String dateTime;
    @SerializedName("end_date")
    private String endDate;
    @SerializedName("end_time")
    private String endTime;
    @SerializedName("event_image_url")
    private String eventImageUrl;
    @SerializedName("event_description")
    private String eventDescription;
    @SerializedName("has_invited")
    private boolean hasInvited;
    @SerializedName("has_rsvped")
    private boolean hasRsvped;
    private int id;
    @SerializedName("invite_description")
    private String inviteDescription;
    @SerializedName("max_add_guests")
    private int maximumAddGuests;
    @SerializedName("my_events_image_url")
    private String myEventsImageUrl;
    @SerializedName("invite_screen_image_url")
    private String inviteImageUrl;
    @SerializedName("remaining_additional_guests")
    private int remainingAdditionalGuests;
    @SerializedName("remaining_slots")
    private int remainingSlots;
    @SerializedName("sponsor_logo_url")
    private String sponsorLogoUrl;
    @SerializedName("start_date")
    private String startDate;
    @SerializedName("start_time")
    private String startTime;
    private String title;
    @SerializedName("view_my_event_image_url")
    private String viewEventImageUrl;
    @SerializedName("is_crowd_sourced")
    private boolean isCrowdSourced;
    @SerializedName("has_expressed_interest")
    private boolean hasExpressedInterest;
    @SerializedName("event_type_id")
    private int eventTypeId;
    private String price;

    public EventModel(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<EventModel> CREATOR = new Creator<EventModel>() {
        @Override
        public EventModel createFromParcel(Parcel in) {
            return new EventModel(in);
        }

        @Override
        public EventModel[] newArray(int size) {
            return new EventModel[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){

        address = in.readString();
        dateTime = in.readString();
        endDate = in.readString();
        endTime = in.readString();
        eventImageUrl = in.readString();
        eventDescription = in.readString();
        hasInvited = in.readByte() == 1;
        hasRsvped = in.readByte() == 1;
        id = in.readInt();
        inviteDescription = in.readString();
        inviteImageUrl = in.readString();
        maximumAddGuests = in.readInt();
        myEventsImageUrl = in.readString();
        remainingAdditionalGuests = in.readInt();
        remainingSlots = in.readInt();
        sponsorLogoUrl = in.readString();
        startDate = in.readString();
        startTime = in.readString();
        title = in.readString();
        viewEventImageUrl = in.readString();
        isCrowdSourced = in.readByte() == 1;
        hasExpressedInterest = in.readByte() == 1;
        eventTypeId = in.readInt();
        price = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(address);
        out.writeString(dateTime);
        out.writeString(endDate);
        out.writeString(endTime);
        out.writeString(eventImageUrl);
        out.writeString(eventDescription);
        out.writeByte((byte) (hasInvited? 1 : 0));
        out.writeByte((byte) (hasRsvped? 1 : 0));
        out.writeInt(id);
        out.writeString(inviteDescription);
        out.writeString(inviteImageUrl);
        out.writeInt(maximumAddGuests);
        out.writeString(myEventsImageUrl);
        out.writeInt(remainingAdditionalGuests);
        out.writeInt(remainingSlots);
        out.writeString(sponsorLogoUrl);
        out.writeString(startDate);
        out.writeString(startTime);
        out.writeString(title);
        out.writeString(viewEventImageUrl);
        out.writeByte((byte) (isCrowdSourced? 1 : 0));
        out.writeByte((byte) (hasExpressedInterest? 1 : 0));
        out.writeInt(eventTypeId);
        out.writeString(price);
    }

    @Bindable
    public String getAddress() {
        return address;
    }

    @Bindable
    public String getDateTime() {
        return dateTime;
    }

    @Bindable
    public String getEndDate() {
        return endDate;
    }

    @Bindable
    public String getEndTime() {
        return endTime;
    }

    @Bindable
    public String getEventImageUrl() {
        return eventImageUrl;
    }

    @Bindable
    public String getEventDescription() {
        return eventDescription;
    }

    @Bindable
    public boolean getHasInvited() {
        return hasInvited;
    }

    @Bindable
    public boolean getHasRsvped() {
        return hasRsvped;
    }

    @Bindable
    public int getId() {
        return id;
    }

    @Bindable
    public String getInviteDescription() {
        return inviteDescription;
    }

    @Bindable
    public String getInviteImageUrl() {
        return inviteImageUrl;
    }

    @Bindable
    public int getMaximumAddGuests() {
        return maximumAddGuests;
    }

    @Bindable
    public String getMyEventsImageUrl() {
        return myEventsImageUrl;
    }

    @Bindable
    public int getRemainingAdditionalGuests() {
        return remainingAdditionalGuests;
    }

    @Bindable
    public int getRemainingSlots() {
        return remainingSlots;
    }

    @Bindable
    public String getSponsorLogoUrl() {
        return sponsorLogoUrl;
    }

    @Bindable
    public String getStartDate() {
        return startDate;
    }

    @Bindable
    public String getStartTime() {
        return startTime;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    @Bindable
    public String getViewEventImageUrl() {
        return viewEventImageUrl;
    }

    public boolean getIsCrowdSourced(){
        return isCrowdSourced;
    }

    @Bindable
    public boolean getHasExpressedInterest(){
        return hasExpressedInterest;
    }

    public int getEventTypeId(){
        return eventTypeId;
    }

    @Bindable
    public String getPrice(){
        return price;
    }

    /**
     * Determine the total number of invited guests
     * by subtracting the {@link #remainingAdditionalGuests} from {@link #maximumAddGuests}
     * @return
     */
    public int getTotalInvitedGuests(){
        int totalInvitedGuests = maximumAddGuests - remainingAdditionalGuests;
        if(totalInvitedGuests < 0){
            return 0;
        }
        return totalInvitedGuests;
    }

    public void setAddress(String address) {
        this.address = address;
        notifyPropertyChanged(BR.address);
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
        notifyPropertyChanged(BR.dateTime);
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
        notifyPropertyChanged(BR.endDate);
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
        notifyPropertyChanged(BR.endTime);
    }

    public void setEventImageUrl(String eventImageUrl) {
        this.eventImageUrl = eventImageUrl;
        notifyPropertyChanged(BR.eventImageUrl);
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
        notifyPropertyChanged(BR.eventDescription);
    }

    public void setHasInvited(boolean hasInvited) {
        this.hasInvited = hasInvited;
        notifyPropertyChanged(BR.hasInvited);
    }

    public void setHasRsvped(boolean hasRsvped) {
        this.hasRsvped = hasRsvped;
        notifyPropertyChanged(BR.hasRsvped);
    }

    public void setId(int id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    public void setInviteDescription(String inviteDescription) {
        this.inviteDescription = inviteDescription;
        notifyPropertyChanged(BR.inviteDescription);
    }

    public void setInviteImageUrl(String inviteImageUrl) {
        this.inviteImageUrl = inviteImageUrl;
        notifyPropertyChanged(BR.inviteImageUrl);
    }

    public void setMaximumAddGuests(int maximumAddGuests) {
        this.maximumAddGuests = maximumAddGuests;
        notifyPropertyChanged(BR.maximumAddGuests);
    }

    public void setMyEventsImageUrl(String myEventsImageUrl) {
        this.myEventsImageUrl = myEventsImageUrl;
        notifyPropertyChanged(BR.myEventsImageUrl);
    }

    public void setRemainingAdditionalGuests(int remainingAdditionalGuests) {
        this.remainingAdditionalGuests = remainingAdditionalGuests;
        notifyPropertyChanged(BR.remainingAdditionalGuests);
    }

    public void setRemainingSlots(int remainingSlots) {
        this.remainingSlots = remainingSlots;
        notifyPropertyChanged(BR.remainingSlots);
    }

    public void setSponsorLogoUrl(String sponsorLogoUrl) {
        this.sponsorLogoUrl = sponsorLogoUrl;
        notifyPropertyChanged(BR.sponsorLogoUrl);
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
        notifyPropertyChanged(BR.startDate);
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
        notifyPropertyChanged(BR.startTime);
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    public void setViewEventImageUrl(String viewEventImageUrl) {
        this.viewEventImageUrl = viewEventImageUrl;
        notifyPropertyChanged(BR.viewEventImageUrl);
    }

    public void setIsCrowdSourced(boolean isCrowdSourced){
        this.isCrowdSourced = isCrowdSourced;
    }

    public void setHasExpressedInterest(boolean hasExpressedInterest){
        this.hasExpressedInterest = hasExpressedInterest;
        notifyPropertyChanged(BR.hasExpressedInterest);
    }

    public void setEventTypeId(int eventTypeId){
        this.eventTypeId = eventTypeId;
    }

    public void setPrice(String price){
        this.price = price;
        notifyPropertyChanged(BR.price);
    }

    public void setAllFields(EventModel other) {
        this.setAddress(other.address);
        this.setDateTime(other.dateTime);
        this.setEndDate(other.endDate);
        this.setEndTime(other.endTime);
        this.setEventImageUrl(other.eventImageUrl);
        this.setEventDescription(other.eventDescription);
        this.setHasRsvped(other.hasRsvped);
        this.setId(other.id);
        this.setInviteDescription(other.inviteDescription);
        this.setInviteImageUrl(other.inviteImageUrl);
        this.setMaximumAddGuests(other.maximumAddGuests);
        this.setMyEventsImageUrl(other.myEventsImageUrl);
        this.setRemainingAdditionalGuests(other.remainingAdditionalGuests);
        this.setRemainingSlots(other.remainingSlots);
        this.setSponsorLogoUrl(other.sponsorLogoUrl);
        this.setStartDate(other.startDate);
        this.setStartTime(other.startTime);
        this.setTitle(other.title);
        this.setViewEventImageUrl(other.viewEventImageUrl);
        this.setIsCrowdSourced(other.isCrowdSourced);
        this.setHasExpressedInterest(other.hasExpressedInterest);
        this.setEventTypeId(other.getEventTypeId());
        this.setPrice(other.getPrice());
    }

    /**
     * Determine if guests have been invited for this event by the user
     * @return true if there's any discrepancy in {@link #remainingAdditionalGuests}
     * and {@link #maximumAddGuests}
     */
    public boolean hasInvitedBasedOnGuestsDifference() {
        return remainingAdditionalGuests != maximumAddGuests;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
