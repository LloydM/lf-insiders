package com.livefitter.theinsiders.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 3/1/17
 * for Livefitter
 */

public class Guest implements Parcelable {

    private String name = "";
    private String salutation = "";

    public Guest(String salutation, String name) {
        this.salutation = salutation;
        this.name = name;
    }

    public Guest(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<Guest> CREATOR = new Creator<Guest>() {
        @Override
        public Guest createFromParcel(Parcel in) {
            return new Guest(in);
        }

        @Override
        public Guest[] newArray(int size) {
            return new Guest[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){
        name = in.readString();
        salutation = in.readString();
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeString(name);
        out.writeString(salutation);
    }

    public String getName() {
        return name;
    }

    public String getSalutation() {
        return salutation;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }

    public String getWholeName(){
        String wholeName = "";
        if(salutation != null && !salutation.isEmpty()) {
            wholeName = wholeName.concat(salutation + " ");
        }

        if(name != null && !name.isEmpty()){
            wholeName = wholeName.concat(name);
        }

        return wholeName;
    }
}
