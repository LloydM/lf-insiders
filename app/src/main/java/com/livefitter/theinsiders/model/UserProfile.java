package com.livefitter.theinsiders.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 2/13/17
 * for Livefitter
 */

public class UserProfile implements Parcelable {
    /*active: true,
    authentication_token: "hNUJIdgQVH9pNVDLXz4b",
    date_of_birth: null,
    email: "jayson@absolute-living.com",
    gender_desc: "Male",
    id: 1,
    mobile_no: "0919 777 8888",
    name: "Jayson Rombaoa",
    qr_code_info_url: "https://the-insiders-dev.herokuapp.com/api/v1/profile/1",
    qr_code_url: "some-url.com",
    salutation: null,
    sign_in_count: 11,
    first_name: "Jayson",
    last_name: "Rambaoa"*/

    private int id;
    private String name = "";
    private String salutation = "";
    private String email = "";
    @SerializedName("authentication_token")
    private String authenticationToken = "";
    @SerializedName("gender_desc")
    private String gender = "";
    @SerializedName("date_of_birth")
    private String dateOfBirth = "";
    @SerializedName("mobile_no")
    private String mobileNumber = "";
    @SerializedName("qr_code_info_url")
    private String qrCodeInfoUrl = "";
    @SerializedName("qr_code_url")
    private String qrCodeUrl = "";
    @SerializedName("sign_in_count")
    private int signInCount;
    @SerializedName("active")
    private boolean isActive;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("is_linkedin")
    private boolean isLinkedIn;

    protected UserProfile(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<UserProfile> CREATOR = new Creator<UserProfile>() {
        @Override
        public UserProfile createFromParcel(Parcel in) {
            return new UserProfile(in);
        }

        @Override
        public UserProfile[] newArray(int size) {
            return new UserProfile[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){
        id = in.readInt();
        name = in.readString();
        salutation = in.readString();
        email = in.readString();
        authenticationToken = in.readString();
        gender = in.readString();
        dateOfBirth = in.readString();
        mobileNumber = in.readString();
        qrCodeInfoUrl = in.readString();
        qrCodeUrl = in.readString();
        signInCount = in.readInt();
        isActive = in.readByte() == 1;
        firstName = in.readString();
        lastName = in.readString();
        isLinkedIn = in.readByte() == 1;
    }

    @Override
    public void writeToParcel(Parcel out, int i) {
        out.writeInt(id);
        out.writeString(name);
        out.writeString(salutation);
        out.writeString(email);
        out.writeString(authenticationToken);
        out.writeString(gender);
        out.writeString(dateOfBirth);
        out.writeString(mobileNumber);
        out.writeString(qrCodeInfoUrl);
        out.writeString(qrCodeUrl);
        out.writeInt(signInCount);
        out.writeByte((byte) (isActive? 1 : 0));
        out.writeString(firstName);
        out.writeString(lastName);
        out.writeByte((byte) (isLinkedIn? 1 : 0));
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSalutation() {
        return salutation;
    }

    public String getEmail() {
        return email;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public String getGender() {
        return gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public String getQrCodeInfoUrl() {
        return qrCodeInfoUrl;
    }

    public String getQrCodeUrl() {
        return qrCodeUrl;
    }

    public int getSignInCount() {
        return signInCount;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isLinkedIn() {
        return isLinkedIn;
    }

    public void setGender(String gender){
        this.gender = gender;
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields){
            try {
                if(f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")){
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
