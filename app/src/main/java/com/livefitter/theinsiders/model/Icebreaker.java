package com.livefitter.theinsiders.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.os.Parcel;
import android.os.Parcelable;

import com.android.databinding.library.baseAdapters.BR;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;


/**
 * Created by LloydM on 3/9/17
 * for Livefitter
 */

public class Icebreaker extends BaseObservable implements Parcelable {

    private String title = "";
    private String description = "";
    @SerializedName("icebreaker_image_url")
    private String imageUrl = "";
    @SerializedName("is_redeemed")
    private boolean isRedeemed = false;

    public Icebreaker(String title, String description, String imageUrl, boolean isRedeemed) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.isRedeemed = isRedeemed;
    }

    protected Icebreaker(Parcel in) {
        readFromParcel(in);
    }

    public static final Creator<Icebreaker> CREATOR = new Creator<Icebreaker>() {
        @Override
        public Icebreaker createFromParcel(Parcel in) {
            return new Icebreaker(in);
        }

        @Override
        public Icebreaker[] newArray(int size) {
            return new Icebreaker[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    private void readFromParcel(Parcel in){
        title = in.readString();
        description = in.readString();
        imageUrl = in.readString();
        isRedeemed = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeString(imageUrl);
        parcel.writeByte((byte) (isRedeemed ? 1 : 0));
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    @Bindable
    public String getDescription() {
        return description;
    }

    @Bindable
    public String getImageUrl() {
        return imageUrl;
    }

    @Bindable
    public boolean isRedeemed() {
        return isRedeemed;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }

    public void setDescription(String description) {
        this.description = description;
        notifyPropertyChanged(BR.description);
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
        notifyPropertyChanged(BR.imageUrl);
    }

    public void setRedeemed(boolean isRedeemed) {
        this.isRedeemed = isRedeemed;
        notifyPropertyChanged(BR.redeemed);
    }

    @Override
    public String toString() {
        String fieldDeclarationString = this.getClass().getSimpleName() + "{ ";
        Field[] fields = this.getClass().getDeclaredFields();
        for (Field f : fields) {
            try {
                if (f.getModifiers() == Modifier.PRIVATE && !f.getName().equals("CREATOR")) {
                    fieldDeclarationString += f.getName() + ": " + f.get(this) + ", \n";
                }
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        fieldDeclarationString += "}";

        return fieldDeclarationString;
    }
}
