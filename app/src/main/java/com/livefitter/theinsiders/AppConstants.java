package com.livefitter.theinsiders;

import android.os.Build;

import com.linkedin.platform.utils.Scope;

/**
 * Created by LloydM on 1/27/17
 * for Livefitter
 */

public class AppConstants {

    // -- Stripe API Key (Publishable key)
    private static final String STRIPE_API_KEY_DEBUG                 = "pk_test_YaWKE3oDNHpvlZVFDHXwPnVO";
    private static final String STRIPE_API_KEY_LIVE                  = "pk_live_1vcWECcBXDKtZkgvpShfk3BN";

    private static final String ApiVersionPath                      = "/api/v1";
    public static final int API_DEVICE_TYPE_ID                      = 1; // 1 = Android, 2 = IOS

    // -- API URLS
    private static final String URL_ALL_EVENTS                      = "/events.json";
    private static final String URL_EVENT_DETAIL                    = "/events";
    private static final String URL_UPCOMING_EVENTS                 = "/events?tag=1";
    private static final String URL_PAST_EVENTS                     = "/events?tag=2";
    private static final String URL_SIGN_UP                         = "/signup";
    private static final String URL_SIGN_UP_LI                      = "/linkedin?mode=1";
    private static final String URL_LOG_IN                          = "/login";
    private static final String URL_LOG_IN_LI                       = "/linkedin?mode=2";
    private static final String URL_LOG_OUT                         = "/login";
    private static final String URL_VERIFY_ACTIVATION_CODE          = "/activate";
    private static final String URL_PROFILE                         = "/profile";
    private static final String URL_INVITE_GUESTS                   = "/guests";
    private static final String URL_RSVP                            = "/rsvp";
    private static final String URL_INVITATIONS                     = "/guests";
    private static final String URL_ICEBREAKER                      = "/icebreaker";
    private static final String URL_ICEBREAKER_REDEEM               = "/redeem";
    private static final String URL_CHANGE_PASSWORD                 = "/change_password";
    private static final String URL_UPDATE_GUEST                    = "/update_guests";
    private static final String URL_UPDATE_PROFILE                  = "/users";
    private static final String URL_INTEREST_EVENTS                 = "/events?tag=3";
    private static final String URL_SEND_MESSAGE                    = "/user_messages";
    private static final String URL_FAQ_TERMS                       = "/faqs";
    private static final String URL_NOTIFICATION_LIST               = "/user_notifications";
    private static final String URL_FORGOT_PASSWORD                 = "/forgot_password";
    private static final String URL_USER_CARDS                      = "/user_cards";
    private static final String URL_ORDER_NUMBER                    = "/user_payments";
    private static final String URL_REMOVE_CARD                     = URL_USER_CARDS + "/remove";
    private static final String URL_PROMO_CODES                     = "/promo_codes";

    // -- LinkedIn API URLs
    private static final String URL_LI_GET_USER_PROFILE_COMPLETE    = "https://api.linkedin.com/v1/people/~:(id,first-name,last-name,email-address)?format=json";
    private static final String URL_LI_GET_USER_PROFILE_SIMPLE      = "https://api.linkedin.com/v1/people/~:(id,email-address)?format=json";

    // -- Fragment tags
    public static String FRAG_CURRENT;
    public static final String FRAG_HOME                            = "Fragment_home";
    public static final String FRAG_MY_EVENTS                       = "Fragment_my_events";
    public static final String FRAG_ICEBREAKER                      = "Fragment_icebreaker";
    public static final String FRAG_PROFILE                         = "Fragment_profile";
    public static final String FRAG_ACTIVATION                      = "Fragment_activation";
    public static final String FRAG_SIGN_UP                         = "Fragment_sign_up";
    public static final String FRAG_LOG_IN                          = "Fragment_log_in";
    public static final String FRAG_EVENT_DETAIL                    = "Fragment_event_detail";
    public static final String FRAG_CROWD_SOURCED_EVENT_DETAIL      = "Fragment_crowd_sourced_event_detail";
    public static final String FRAG_SUPPORT                         = "Fragment_support";
    public static final String FRAG_NOTIFICATION                    = "Fragment_notification";
    public static final String FRAG_PAID_EVENT_DETAIL               = "Fragment_paid_event_detail";
    public static final String FRAG_PRICE_SUMMARY                   = "Fragment_price_summary";
    public static final String FRAG_CREDIT_CARD_LIST                = "Fragment_credit_card_list";
    public static final String FRAG_ADD_CREDIT_CARD                 = "Fragment_add_credit_card";
    public static final String FRAG_ORDER_CONFIRMED                 = "Fragment_order_confirmed";

    // -- Shared Preferences
    public static final String PREFS_MAIN                           = "com.livefitter.theinsiders-preferences";
    public static final String PREFS_KEY_HAS_CHOSEN_SALUTATIONS     = "com.livefitter.theinsiders-has_chosen_salutations";
    public static final String PREFS_KEY_KNOWS_INVITE_INSTRUCTIONS  = "com.livefitter.theinsiders-knows_invite_instructions";
    public static final String PREFS_KEY_USER_AUTH_TOKEN            = "com.livefitter.theinsiders-auth_token";
    public static final String PREFS_KEY_LI_AUTH_TOKEN              = "com.livefitter.theinsiders-linkedin_auth_token";
    public static final String PREFS_KEY_LI_AUTH_TOKEN_EXPIRY       = "com.livefitter.theinsiders-linkedin_auth_token_expiry";
    public static final String PREFS_KEY_SHOULD_DISPLAY_WALKTHROUGH = "com.livefitter.theinsiders-should_display_walkthrough";

    // -- Bundle (Intent/Fragment argument) keys
    public static final String KEY_ACTIVATION_CODE                  = "key-activation_code";
    public static final String KEY_USER_PROFILE                     = "key-user_profile";
    public static final String KEY_EVENT_MODEL                      = "key-event_model";
    public static final String KEY_EVENT_EXPIRED                    = "key-event_expired?";
    public static final String KEY_ICEBREAKER                       = "key-icebreaker";
    public static final String KEY_NOTIFICATION_ID                  = "key-notification_id";
    public static final String KEY_CHECK_OUT_MODE                   = "key-check_out_mode";
    public static final String KEY_IMAGE_RESOURCE                   = "key-image_resource";
    public static final String KEY_INVITED_GUEST_LIST               = "key-invited_guest_list";
    public static final String KEY_PROMO_CODE                       = "key-promo_code";

    // -- Intent Request Codes
    public static final int REQUEST_CODE_ICEBREAKER                 = 1557;
    public static final int REQUEST_CODE_UPDATE_PROFILE             = 1558;
    public static final int REQUEST_CODE_ADD_CREDIT_CARD            = 1559;

    // -- Permission Request Codes
    public static final int PERM_SHARE_INVITATION                   = 1660;

    // -- Date formats
    public static final String DATE_BIRTHDATE_FORMAT                = "dd/MM/yyyy";

    // -- Event types
    public static final int EVENT_TYPE_NORMAL                       = 1;
    public static final int EVENT_TYPE_PAID                         = 2;
    public static final int EVENT_TYPE_CROWD_SOURCED                = 3;

    /**
     * Build the list of member permissions our LinkedIn session requires
     * We only need their basic profile and email address
     */
    public static final Scope LINKEDIN_SCOPE                        = Scope.build(Scope.R_EMAILADDRESS, Scope.R_BASICPROFILE);

    // -- Notifications
    public static final int NOTIF_DEFAULT_ID                        = 123;
    public static final String NOTIF_CHANNEL_ID                            = "com.livefitter.theinsiders.notification";
    public static final String NOTIF_CHANNEL_NAME                          = "INSIDERS CHANNEL";

    public static String getStripeApiKey(){
        if (BuildConfig.STRIPE_KEY.equals("live")) {
            return STRIPE_API_KEY_LIVE;
        }else{
            return STRIPE_API_KEY_DEBUG;
        }
    }

    // -- URL getters for each API request

    /**
     * Get the appropriate environment URL path
     */
    private static String getEnvironmentUrl(){
        /*if(ENVIRONMENT.equals(ENVIRONMENT_PRODUCTION)){
            return "https://the-insiders.herokuapp.com";
        }

        return "https://the-insiders-dev.herokuapp.com";*/

        return BuildConfig.BASE_API_ENDPOINT;
    }

    /**
     * Concatinates the functionUrl with the environmentUrl and API version path
     * in the following format: [environment URL][API version path][function URL]
     * @param functionUrl
     * @return complete URL environment, API and function paths.
     */
    private static String apiUrl(String functionUrl){
        return getEnvironmentUrl() + ApiVersionPath + functionUrl;
    }

    /**
     * URL for API call to retrieve list of events
     */
    public static String urlAllEvents(){
        return apiUrl(URL_ALL_EVENTS);
    }

    /**
     * URL for API call to retrieve all details of an event.
     * You must provide a eventId after this url (e.g.g "/12")
     */
    public static String urlEventDetail(){
        return apiUrl(URL_EVENT_DETAIL);
    }


    /**
     * URL for API call to retrieve all upcoming events
     */
    public static String urlUpcomingEvents(){
        return apiUrl(URL_UPCOMING_EVENTS);
    }


    /**
     * URL for API call to retrieve all past events
     */
    public static String urlPastEvents(){
        return apiUrl(URL_PAST_EVENTS);
    }

    /**
     * URL for API call to verify a provided activation code
     */
    public static String urlVerifyActivationCode(){
        return apiUrl(URL_VERIFY_ACTIVATION_CODE);
    }

    /**
     * URL for API call to sign up the user manually
     */
    public static String urlSignUp(){
        return apiUrl(URL_SIGN_UP);
    }

    /**
     * URL for API call to sign up the user via LinkedIn
     */
    public static String urlSignUpLinkedIn(){
        return apiUrl(URL_SIGN_UP_LI);
    }

    /**
     * URL for API call to log in the user manually
     */
    public static String urlLogIn(){
        return apiUrl(URL_LOG_IN);
    }

    /**
     * URL for API call to log in the user via LinkedIn
     */
    public static String urlLogInLinkedIn(){
        return apiUrl(URL_LOG_IN_LI);
    }

    /**
     * URL LinkedIn API call to retrieve the complete profile fields of the user:
     * <ul>
     *     <li>Id</li>
     *     <li>First Name</li>
     *     <li>Last Name</li>
     *     <li>Email</li>
     * </ul>
     */
    public static String linkedInUrlCompleteProfileFields(){
        return URL_LI_GET_USER_PROFILE_COMPLETE;
    }

    /**
     * URL LinkedIn API call to retrieve the simplified profile fields of the user
     * <ul>
     *     <li>Id</li>
     *     <li>Email</li>
     * </ul>
     */
    public static String linkedInUrlSimpleProfileFields(){
        return URL_LI_GET_USER_PROFILE_SIMPLE;
    }

    /**
     * URL for API call to log out the user from the server
     */
    public static String urlLogOut(){
        return apiUrl(URL_LOG_OUT);
    }

    /**
     * URL for API call to update the user's salutation
     */
    public static String urlUpdateSalutation(){
        return apiUrl(URL_LOG_IN);
    }


    /**
     * URL for API call to retrieve the profile of the currently logged in user
     */
    public static String urlRetrieveUserProfile(){
        return apiUrl(URL_PROFILE);
    }


    /**
     * URL for API call to RSVP the user and (optional) invite a list of guests
     */
    public static String urlRsvp(){
        return apiUrl(URL_RSVP);
    }

    /**
     * URL for API call to invite a list of guests
     */
    public static String urlInviteGuests(){
        return apiUrl(URL_INVITE_GUESTS);
    }

    /**
     * URL for API call to get a list of all invite for a given event
     */
    public static String urlEventInvitations(){
        return apiUrl(URL_INVITATIONS);
    }

    /**
     * URL for API call to retrieve the icebreaker, if available
     */
    public static String urlIcebreaker(){
        return apiUrl(URL_ICEBREAKER);
    }

    /**
     * URL for API call to redeem the icebreaker
     */
    public static String urlIcebreakerRedeem(){
        return apiUrl(URL_ICEBREAKER_REDEEM);
    }

    /**
     * URL for API call to change the user's password
     */
    public static String urlChangePassword(){
        return apiUrl(URL_CHANGE_PASSWORD);
    }

    /**
     * URL for API call to update guest name and salutation
     */
    public static String urlUpdateGuest(){
        return apiUrl(URL_UPDATE_GUEST);
    }

    /**
     * URL for API call to update user profile
     */
    public static String urlUpdateProfile(){
        return apiUrl(URL_UPDATE_PROFILE);
    }

    /**
     * URL for API call to get user interested events
     */
    public static String urlInterestEvents(){
        return apiUrl(URL_INTEREST_EVENTS);
    }

    /**
     * URL for API call to send message
     */
    public static String urlSendMessage(){
        return apiUrl(URL_SEND_MESSAGE);
    }

    /**
     * URL for API call to get faq and terms and condition content
     */
    public static String urlFaqTerms(){
        return apiUrl(URL_FAQ_TERMS);
    }

    /**
     * URL for API call to get notifications list
     */
    public static String urlNotificationList(){
        return apiUrl(URL_NOTIFICATION_LIST);
    }

    /**
     * URL for API call to reset password
     */
    public static String urlForgotPassword(){
        return apiUrl(URL_FORGOT_PASSWORD);
    }

    /**
     * URL for API call to get credit card list or add a new card
     */
    public static String urlUserCards(){
        return apiUrl(URL_USER_CARDS);
    }

    /**
     * URL for API call to get latest order number
     */
    public static String urlOrderNumber(){
        return apiUrl(URL_ORDER_NUMBER);
    }

    /**
     * URL for API call to remove card
     */
    public static String urlRemoveCard(){
        return apiUrl(URL_REMOVE_CARD);
    }

    /**
     * URL for API call to validate a promo code
     */
    public static String urlPromoCode(){
        return apiUrl(URL_PROMO_CODES);
    }

}
