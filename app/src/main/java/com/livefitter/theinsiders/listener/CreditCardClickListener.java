package com.livefitter.theinsiders.listener;

import com.livefitter.theinsiders.model.CreditCard;

/**
 * Created by LFT-PC-010 on 7/17/2017.
 */

public interface CreditCardClickListener {

    void onCardClick(CreditCard card);

}
