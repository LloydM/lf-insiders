package com.livefitter.theinsiders.listener;

/**
 * Created by LloydM on 2/15/17
 * for Livefitter
 */

public interface LinkedInSessionListener {
    /**
     * LinkedIn session is authenticated and ready to accept additional API calls
     */
    public void onLinkedInSessionReady();

    /**
     * LinkedIn session authentication is invalid.
     * Common causes is the wrong debug/release key hash not matching the one in the LinkedIn project
     */
    public void onLinkedInSessionError(String message);
}
