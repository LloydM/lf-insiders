package com.livefitter.theinsiders.listener;

import com.livefitter.theinsiders.model.EventModel;

/**
 * Created by LloydM on 1/30/17
 * for Livefitter
 */

public interface EventClickListener {
    public void onEventClicked(EventModel eventModel);
}
