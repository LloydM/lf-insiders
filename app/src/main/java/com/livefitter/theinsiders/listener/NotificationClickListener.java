package com.livefitter.theinsiders.listener;

import com.livefitter.theinsiders.model.Notification;

/**
 * Created by LFT-PC-010 on 6/27/2017.
 */

public interface NotificationClickListener {

    void onClick(Notification notification);

}
